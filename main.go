package main

import (
	"2arc/core"
	"flag"
)

func main() {
	var configFile = flag.String("conf", "./config.conf", "The configuration file to use")
	var pcap = flag.Bool("pcap", false, "If true, the firewall will filter pcap-formatted packets, instead of doing live filtering with the kernel.")
	var pcapFile = flag.String("pcap-file", "/dev/stdin", "The capture file to read packets from, used only when -pcap is set to true.")
	flag.Parse()

	if *pcap {
		core.OffWire(*configFile, *pcapFile)
	} else {
		core.Live(*configFile)
	}
}
