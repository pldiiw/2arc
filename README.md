# 2ARC — Firewall

Documentation is in doc/ folder. It has been exported to html for convenience,
but to also access the modules documention, you will need to install `godoc`.

```
go get -v golang.org/x/tools/cmd/godoc
godoc -http:6060
```

Then, open your web browser and point your URL to `http://localhost:6060/pkg/2arc`.
