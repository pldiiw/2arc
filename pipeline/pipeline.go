package pipeline

import (
	"2arc/config"
	"2arc/modules/ethernet"
	"2arc/modules/icmp4"
	"2arc/modules/ipv4"
	"2arc/modules/ipv6"
	"2arc/modules/stub"
	"2arc/modules/tcp"
	"2arc/modules/udp"
	"github.com/google/gopacket"
	"github.com/pkg/errors"
)

var modules = map[string](map[string]func(string) (func(gopacket.Packet) bool, error)){
	"stub":     stub.Predicates,
	"tcp":      tcp.Predicates,
	"udp":      udp.Predicates,
	"ipv4":     ipv4.Predicates,
	"ipv6":     ipv6.Predicates,
	"ethernet": ethernet.Predicates,
	"icmp4":    icmp4.Predicates,
}

// Init initializes the processing pipelines
func Init(configuration config.Configuration) (map[string]map[string]func(gopacket.Packet) bool, error) {
	pipelineZones := make(map[string]map[string]func(gopacket.Packet) bool)

	for i := range configuration.Zones {
		zone := configuration.Zones[i]

		pipelineZoneChains := make(map[string]func(gopacket.Packet) bool)

		for i := range zone.Chains {
			chain := zone.Chains[i]

			var pipelineZoneChain func(gopacket.Packet) bool

			pipelineZoneChainRules := make([]func(gopacket.Packet) string, 0)

			for i := range chain.Rules {
				rule := chain.Rules[i]

				var pipelineZoneChainRule func(gopacket.Packet) string

				pipelineZoneChainRuleConditions := make([]func(gopacket.Packet) bool, 0)

				for i := range rule.Conditions {
					condition := rule.Conditions[i]

					var pipelineZoneChainRuleCondition func(gopacket.Packet) bool

					module := modules[condition.Module]
					if module == nil {
						return nil, errors.Errorf("Could not find module %s", condition.Module)
					}

					predicate := module[condition.Predicate]
					if predicate == nil {
						return nil, errors.Errorf("could not find predicate %s in module %s", condition.Predicate, condition.Module)
					}

					pipelineZoneChainRuleCondition, err := predicate(condition.Expression)
					if err != nil {
						return nil, errors.Errorf("Predicate %s/%s could not be instanciated: %v", condition.Module, condition.Predicate, err)
					}

					pipelineZoneChainRuleConditions = append(pipelineZoneChainRuleConditions, pipelineZoneChainRuleCondition)
				}

				pipelineZoneChainRule = func(packet gopacket.Packet) string {
					for i := range pipelineZoneChainRuleConditions {
						if !pipelineZoneChainRuleConditions[i](packet) {
							return ""
						}
					}

					return rule.Action
				}
				pipelineZoneChainRules = append(pipelineZoneChainRules, pipelineZoneChainRule)
			}

			pipelineZoneChain = func(packet gopacket.Packet) bool {
				for i := range pipelineZoneChainRules {
					switch action := pipelineZoneChainRules[i](packet); action {
					case "":
						continue
					case config.Accept:
						return true
					case config.Drop:
						return false
					default:
						return pipelineZoneChains[action](packet)
					}
				}

				return false // bool zero value, we shouldn't ever reach this line
			}

			pipelineZoneChains[chain.Name] = pipelineZoneChain
		}

		pipelineZones[zone.Name] = pipelineZoneChains
	}

	return pipelineZones, nil
}
