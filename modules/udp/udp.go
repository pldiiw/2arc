package udp

import (
	"errors"
	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
	"strconv"
	"strings"
)

var Predicates = map[string]func(expr string) (func(gopacket.Packet) bool, error){
	"Source":           Source,
	"SourceRange":      SourceRange,
	"SourceNot":        SourceNot,
	"Destination":      Destination,
	"DestinationRange": DestinationRange,
	"DestinationNot":   DestinationNot,
	"LengthMax":        LengthMax,
	"LengthMin":        LengthMin,
}

//Source matches when a UDP packet has its source port field corresponding to the port given as an expression.
//The expression that this predicate expects is a number between 0 and 65535 corresponding to a UDP port.
func Source(expr string) (func(gopacket.Packet) bool, error) {

	parsedExpr, err := strconv.ParseInt(expr, 10, 16)
	if err != nil {
		return nil, err
	}
	srcPort := layers.UDPPort(parsedExpr)
	return (func(packet gopacket.Packet) bool {

		if udpLayer := packet.Layer(layers.LayerTypeUDP); udpLayer == nil {
			return false
		} else {
			return udpLayer.(*layers.UDP).SrcPort == srcPort
		}
	}), nil
}

//SourceRange matches when a UDP packet has its source port field is included in a range of ports given as an expression.
//The expression that this predicate expects is two numbers between 0 and 65535 corresponding to UDP ports, the numbers must be
//different, separated by a dash, and the first one must be lesser than the second one.
//Exemple : 10-500
func SourceRange(expr string) (func(gopacket.Packet) bool, error) {
	split := strings.Index(expr, "-")
	if split == -1 {
		return nil, errors.New("no '-' find")
	}

	first, err := strconv.ParseInt(expr[:split], 10, 16)
	if err != nil {
		return nil, err
	}
	last, err := strconv.ParseInt(expr[split+1:], 10, 16)
	if err != nil {
		return nil, err
	}
	srcFirstPort := layers.UDPPort(first)
	srcLastPort := layers.UDPPort(last)

	return (func(packet gopacket.Packet) bool {
		if udpLayer := packet.Layer(layers.LayerTypeUDP); udpLayer == nil {
			return false
		} else {
			return udpLayer.(*layers.UDP).SrcPort >= srcFirstPort && udpLayer.(*layers.UDP).SrcPort < srcLastPort
		}
	}), nil
}

// SourceNot is Source's complement.
func SourceNot(expr string) (func(gopacket.Packet) bool, error) {
	f, err := Source(expr)
	if err != nil {
		return nil, err
	}

	return (func(packet gopacket.Packet) bool {
		return !f(packet)
	}), nil
}

//Destination matches when a UDP packet has its destination port field corresponding to the port given as an expression.
//The expression that this predicate expects is a number between 0 and 65535 corresponding to a UDP port.
func Destination(expr string) (func(gopacket.Packet) bool, error) {
	parsedExpr, err := strconv.ParseInt(expr, 10, 16)
	if err != nil {
		return nil, err
	}
	dstPort := layers.UDPPort(parsedExpr)
	return (func(packet gopacket.Packet) bool {
		if udpLayer := packet.Layer(layers.LayerTypeUDP); udpLayer == nil {
			return false
		} else {
			return udpLayer.(*layers.UDP).DstPort == dstPort
		}
	}), nil
}

//DestinationRange matches when a UDP packet has its destination port field is included in a range of ports given as an expression.
//The expression that this predicate expects is two numbers between 0 and 65535 corresponding to UDP ports, the numbers must be
//different, separated by a dash, and the first one must be lesser than the second one.
//Exemple : 10-500
func DestinationRange(expr string) (func(gopacket.Packet) bool, error) {
	split := strings.Index(expr, "-")
	if split == -1 {
		return nil, errors.New("no '-' find")
	}

	first, err := strconv.ParseInt(expr[:split], 10, 16)
	if err != nil {
		return nil, err
	}
	last, err := strconv.ParseInt(expr[split+1:], 10, 16)
	if err != nil {
		return nil, err
	}

	dstFirstPort := layers.UDPPort(first)
	dstLastPort := layers.UDPPort(last)

	return (func(packet gopacket.Packet) bool {
		if udpLayer := packet.Layer(layers.LayerTypeUDP); udpLayer == nil {
			return false
		} else {
			return udpLayer.(*layers.UDP).DstPort >= dstFirstPort && udpLayer.(*layers.UDP).DstPort < dstLastPort
		}
	}), nil
}

// DestinationNot is Destination's complement.
func DestinationNot(expr string) (func(gopacket.Packet) bool, error) {
	f, err := Destination(expr)
	if err != nil {
		return nil, err
	}

	return (func(packet gopacket.Packet) bool {
		return !f(packet)
	}), nil
}

//LengthMax matches when a UDP packet has its length field lesser than a number given as an expression.
//The expression that this predicate expects is a number representing the maximum length of an UDP packet.
func LengthMax(expr string) (func(gopacket.Packet) bool, error) {
	parsedExpr, err := strconv.ParseUint(expr, 10, 16)
	if err != nil {
		return nil, err
	}
	maxLength := uint16(parsedExpr)

	return (func(packet gopacket.Packet) bool {
		if udpLayer := packet.Layer(layers.LayerTypeUDP); udpLayer == nil {
			return false
		} else {
			return udpLayer.(*layers.UDP).Length <= maxLength
		}
	}), nil
}

//LengthMin matches when a UDP packet has its length field higher than a number given as an expression.
//The expression that this predicate expects is a number representing the minimum length of an UDP packet.
func LengthMin(expr string) (func(gopacket.Packet) bool, error) {
	parsedExpr, err := strconv.ParseUint(expr, 10, 16)
	if err != nil {
		return nil, err
	}
	minLength := uint16(parsedExpr)

	return (func(packet gopacket.Packet) bool {
		if udpLayer := packet.Layer(layers.LayerTypeUDP); udpLayer == nil {
			return false
		} else {
			return udpLayer.(*layers.UDP).Length >= minLength
		}
	}), nil
}
