package udp

import (
	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
	"os"
	"testing"
)

var testPacket gopacket.Packet
var testTCPPacket gopacket.Packet
var testNoTransportPacket gopacket.Packet

func TestMain(m *testing.M) {
	buf := gopacket.NewSerializeBuffer()
	opts := gopacket.SerializeOptions{}

	gopacket.SerializeLayers(buf, opts, &layers.UDP{})
	testPacket = gopacket.NewPacket(buf.Bytes(), layers.LayerTypeUDP, gopacket.Default)
	testPacket.Layer(layers.LayerTypeUDP).(*layers.UDP).SrcPort = layers.UDPPort(500)
	testPacket.Layer(layers.LayerTypeUDP).(*layers.UDP).DstPort = layers.UDPPort(501)
	testPacket.Layer(layers.LayerTypeUDP).(*layers.UDP).Length = uint16(100)

	gopacket.SerializeLayers(buf, opts, &layers.TCP{})
	testTCPPacket = gopacket.NewPacket(buf.Bytes(), layers.LayerTypeTCP, gopacket.Default)

	gopacket.SerializeLayers(buf, opts, &layers.Ethernet{}, &layers.IPv4{})
	testNoTransportPacket = gopacket.NewPacket(buf.Bytes(), layers.LayerTypeIPv4, gopacket.Default)

	os.Exit(m.Run())
}

func TestSourceTrue(t *testing.T) {
	t.Log(`Passing "500" as an expression to Source should match an UDP segment having its source port field set to 500`)
	f, err := Source("500")
	if err != nil {
		t.Errorf(`Source could not parse "500": %v`, err)
	}

	if !f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestSourceFalse(t *testing.T) {
	t.Log(`Passing "1" as an expression to Source shouldn't match an UDP segment having its source port field not set to 1`)
	f, err := Source("1")
	if err != nil {
		t.Errorf(`Source could not parse "1": %v`, err)
	}

	if f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestSourceErr(t *testing.T) {
	t.Log(`Passing anything else than a source port id to Source should return an error.`)

	_, err := Source("fail")

	if err == nil {
		t.Error(`SourceNot parsed "fail" as a valid expression, it shouldn't: only a number is valid.`)
	}
}

func TestSourceRangeTrue(t *testing.T) {
	t.Log(`Passing "500-510" as an expression to SourceRange should match an UDP segment having its source port field included 
between 500 and 510`)
	f, err := SourceRange("500-510")
	if err != nil {
		t.Errorf(`SourceRange could not parse "500-510": %v`, err)
	}

	if !f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestSourceRangeFalse(t *testing.T) {
	t.Log(`Passing "1-2" as an expression to SourceRange shouldn't match an UDP segment having its source port field not included 
between 1 and 2`)
	f, err := SourceRange("1-2")
	if err != nil {
		t.Errorf(`SourceRange could not parse "1-2": %v`, err)
	}

	if f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestSourceRangeErr(t *testing.T) {
	t.Log(`Passing anything else than two source port id separated by a dash to SourceRange should return an error.`)

	_, err := SourceRange("fail")

	if err == nil {
		t.Error(`SourceRange parsed "fail" as a valid expression, it shouldn't: only two numbers separated by a dash is valid`)
	}
}

func TestSourceNotTrue(t *testing.T) {
	t.Log(`Passing "1" as an expression to SourceNot should match an UDP segment having its source port field not set to 1`)
	f, err := SourceNot("1")
	if err != nil {
		t.Errorf(`SourceNot could not parse "1": %v`, err)
	}

	if !f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestSourceNotFalse(t *testing.T) {
	t.Log(`Passing "500" as an expression to SourceNot shouldn't match an UDP segment having its source port field set to 500`)
	f, err := SourceNot("500")
	if err != nil {
		t.Errorf(`SourceNot could not parse "500": %v`, err)
	}

	if f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestSourceNotErr(t *testing.T) {
	t.Log(`Passing anything else than source port id to SourceNot should return an error.`)

	_, err := SourceNot("fail")

	if err == nil {
		t.Error(`SourceNot parsed "fail" as a valid expression, it shouldn't: only a number is valid.`)
	}
}

func TestDestinationTrue(t *testing.T) {
	t.Log(`Passing "501" as an expression to Destination should should match an UDP segment having its destination port field set to 501`)
	f, err := Destination("501")
	if err != nil {
		t.Errorf(`Destination could not parse "501": %v`, err)
	}

	if !f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestDestinationFalse(t *testing.T) {
	t.Log(`Passing "1" as an expression to Destiantion shouldn't match an UDP segment having its destination port field not set
to 1`)
	f, err := Destination("1")
	if err != nil {
		t.Errorf(`Destination could not parse "1": %v`, err)
	}

	if f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestDestinationErr(t *testing.T) {
	t.Log(`Passing anything else than a destination port id to Destination should return an error.`)

	_, err := Destination("fail")

	if err == nil {
		t.Error(`Destination parsed "fail" as a valid expression, it shouldn't: only a number is valid.`)
	}
}

func TestDestinationRangeTrue(t *testing.T) {
	t.Log(`Passing "500-510" as an expression to DestinationRange should should match an UDP segment having its destination port 
field included between 500 and 510`)
	f, err := DestinationRange("500-510")
	if err != nil {
		t.Errorf(`DestinationRange could not parse "500-510": %v`, err)
	}

	if !f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestDestinationRangeFalse(t *testing.T) {
	t.Log(`Passing "1-2" as an expressionto DestiantionRange shouldn't match an UDP segment having its destination port field 
not included between 1 and 2`)
	f, err := DestinationRange("1-2")
	if err != nil {
		t.Errorf(`DestinationRange could not parse "1-2": %v`, err)
	}

	if f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestDestinationRangeErr(t *testing.T) {
	t.Log(`Passing anything else than two destination port id separated by a dash to DestinationRange should return an error.`)

	_, err := DestinationRange("fail")

	if err == nil {
		t.Error(`DestinationRange parsed "fail" as a valid expression, it shouldn't: only two numbers separated by a dash is valid.`)
	}
}

func TestDestinationNotTrue(t *testing.T) {
	t.Log(`Passing "1" as an expression should match an UDP segment having its destination port field not set to 1`)
	f, err := DestinationNot("1")
	if err != nil {
		t.Errorf(`DestinationNot could not parse "1": %v`, err)
	}

	if !f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestDestinationNotFalse(t *testing.T) {
	t.Log(`Passing "501" as an expression to DestiantionNot shouldn't match an UDP segment having its destination port field set
to 501`)
	f, err := DestinationNot("501")
	if err != nil {
		t.Errorf(`DestinationNot could not parse "501": %v`, err)
	}

	if f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestDestinationNotErr(t *testing.T) {
	t.Log(`Passing anything else than a destination port id to DestinationNot should return an error.`)

	_, err := DestinationNot("fail")

	if err == nil {
		t.Error(`DestinationNot parsed "fail" as a valid expression, it shouldn't: only a number is valid.`)
	}
}

func TestLengthMaxTrue(t *testing.T) {
	t.Log(`Passing "500" as an expression to LengthMax should match an UDP segment having its lenght field set to 500 or less`)
	f, err := LengthMax("500")
	if err != nil {
		t.Errorf(`LengthMax could not parse "500": %v`, err)
	}

	if !f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestLengthMaxFalse(t *testing.T) {
	t.Log(`Passing "1" as an expression to LengthMax shouldn't match an UDP segment having its length field not set to 1 or less`)
	f, err := LengthMax("1")
	if err != nil {
		t.Errorf(`LengthMax could not parse "1": %v`, err)
	}

	if f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestLengthMaxErr(t *testing.T) {
	t.Log(`Passing anything else than a number to LengthMax should return an error.`)

	_, err := LengthMax("fail")

	if err == nil {
		t.Error(`LengthMax parsed "fail" as a valid expression, it shouldn't: only a number is valid.`)
	}
}

func TestLengthMinTrue(t *testing.T) {
	t.Log(`Passing "1" as an expression to LengthMin should match an UDP segment having its length field set to 1 or more`)
	f, err := LengthMin("1")
	if err != nil {
		t.Errorf(`LengthMin could not parse "1": %v`, err)
	}

	if !f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestLengthMinFalse(t *testing.T) {
	t.Log(`Passing "500" as an expression to Destiantion shouldn't match an UDP segment having its length field not set to 500 
or more`)
	f, err := LengthMin("500")
	if err != nil {
		t.Errorf(`LengthMin could not parse "500": %v`, err)
	}

	if f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestLengthMinErr(t *testing.T) {
	t.Log(`Passing anything else than a number to LengthMin should return an error.`)

	_, err := LengthMin("fail")

	if err == nil {
		t.Error(`LengthMin parsed "fail" as a valid expression, it shouldn't: only a number is valid.`)
	}
}

func TestLackingUDPLayer(t *testing.T) {
	t.Log(`Passing a packet not having an UDP datagram should return false for all predicates.`)

	for expr, fn := range map[string]func(string) (func(packet gopacket.Packet) bool, error){
		"500":     Source,
		"480-510": SourceRange,
		"501":     Destination,
		"481-510": DestinationRange,
		"1":       LengthMin,
		"400":     LengthMax,
	} {
		f, err := fn(expr)
		if err != nil {
			t.Error(err)
		}
		if f(testTCPPacket) {
			t.Error(`Packet shouldn't have matched: the transport isn't UDP`)
		}
		if f(testNoTransportPacket) {
			t.Error(`Packet shouldn't have matched: there's no transport layer`)
		}
	}

}
