package stub

import (
	"github.com/google/gopacket"
	"github.com/pkg/errors"
)

var Predicates = map[string]func(expr string) (func(gopacket.Packet) bool, error){
	"Match": Match,
}

func Match(expr string) (func(gopacket.Packet) bool, error) {
	var yes bool

	switch expr {
	case "yes":
		yes = true
	case "no":
		yes = false
	default:
		return nil, errors.Errorf(`Couldn't parse expr "%s", should be "yes" or "no"`, expr)
	}

	return func(packet gopacket.Packet) bool {
		return yes
	}, nil
}
