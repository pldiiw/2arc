package stub

import (
	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
	"os"
	"testing"
)

var testPacket gopacket.Packet

func TestMain(m *testing.M) {
	buf := gopacket.NewSerializeBuffer()
	opts := gopacket.SerializeOptions{}
	gopacket.SerializeLayers(buf, opts, &layers.Ethernet{})

	testPacket = gopacket.NewPacket(buf.Bytes(), layers.LayerTypeEthernet, gopacket.Default)

	os.Exit(m.Run())
}

func TestMatchYes(t *testing.T) {
	t.Log(`Passing "yes" as an expression should return a function that always evaluates to true`)
	yes, err := Match("yes")

	if err != nil {
		t.Errorf(`Match could not parse "yes": %v`, err)
	}

	if !yes(testPacket) {
		t.Error(`Match returned's fn evaluates to false when given yes, it should evaluates to true`)
	}

}

func TestMatchNo(t *testing.T) {
	t.Log(`Passing "no" as an expression should return a function that always evaluates to false`)
	no, err := Match("no")

	if err != nil {
		t.Errorf(`Match could not parse "no": %v`, err)
	}

	if no(testPacket) {
		t.Error(`Match returned's fn evaluates to true when given no, it should evaluates to false`)
	}

}

func TestMatchInvalidExpr(t *testing.T) {
	t.Log(`Passing anything else than "yes" or "no" should return an error.`)

	_, err := Match("fail")

	if err == nil {
		t.Error(`Match parsed "fail" as a valid expression, it shouldn't: only yes and no are valid.`)
	}
}
