package ipv6

import (
	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
	"net"
	"os"
	"strconv"
	"testing"
)

var testPacket gopacket.Packet
var testIPv4Packet gopacket.Packet
var testNoIpPacket gopacket.Packet


func TestMain(m *testing.M) {
	ttl, _ := strconv.ParseUint("100", 10, 8)

	length, _ := strconv.ParseUint("300", 10, 16)

	buf := gopacket.NewSerializeBuffer()
	opts := gopacket.SerializeOptions{}
	gopacket.SerializeLayers(buf, opts, &layers.IPv6{
		SrcIP: net.ParseIP("2001:db8:a0b:12f0::1"),
		DstIP: net.ParseIP("2001:db8:a0b:12f0::2"),
		HopLimit: uint8(ttl),
		Length:   uint16(length),
	})

	testPacket = gopacket.NewPacket(buf.Bytes(), layers.LayerTypeIPv6, gopacket.Default)


	gopacket.SerializeLayers(buf, opts, &layers.IPv4{})
	testIPv4Packet = gopacket.NewPacket(buf.Bytes(), layers.LayerTypeIPv4, gopacket.Default)

	gopacket.SerializeLayers(buf, opts, &layers.Ethernet{}, &layers.TCP{})
	testNoIpPacket = gopacket.NewPacket(buf.Bytes(), layers.LayerTypeTCP, gopacket.Default)



	os.Exit(m.Run())
}

func TestSourceTrue(t *testing.T) {
	t.Log(`Passing "2001:db8:a0b:12f0::1" as an expression to Source should match an IPv6 segment having its source ip field set to "2001:db8:a0b:12f0::1"`)
	f, err := Source("2001:db8:a0b:12f0::1")
	if err != nil {
		t.Errorf(`Source could not parse "2001:db8:a0b:12f0::1": %v`, err)
	}

	if !f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestSourceFalse(t *testing.T) {
	t.Log(`Passing "2001:db8:a0b:12f0::2" as an expression to Source shouldn't match an IPv6 segment having its source ip field not set to "2001:db8:a0b:12f0::2"`)
	no, err := Source("2001:db8:a0b:12f0::2")
	if err != nil {
		t.Errorf(`Source could not parse "2001:db8:a0b:12f0::2": %v`, err)
	}

	if no(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestSourceErr(t *testing.T) {
	t.Log(`Passing anything else than a number should return an error.`)

	_, err := Source("fail")

	if err == nil {
		t.Error(`Source parsed "fail" as a valid expression, it shouldn't: only an ip address is valid.`)
	}
}

func TestSouceNotTrue(t *testing.T) {

	t.Log(`Passing "2001:db8:a0b:12f0::2"as an expression to SourceNot should match an IPv6 segment having its source ip field not set to "2001:db8:a0b:12f0::2"`)
	yes, err := SourceNot("2001:db8:a0b:12f0::2")
	if err != nil {
		t.Errorf(`SourceNot could not parse "2001:db8:a0b:12f0::2": %v`, err)
	}

	if !yes(testPacket) {
		t.Error(`The given packet didn't match.`)
	}
}

func TestSourceNotFalse(t *testing.T) {

	t.Log(`Passing "2001:db8:a0b:12f0::1" as an expression to SourceNot shouldn't match an IPv6 segment having its source ip field set to "2001:db8:a0b:12f0::1"`)
	yes, err := SourceNot("2001:db8:a0b:12f0::1")
	if err != nil {
		t.Errorf(`SourceNot could not parse "2001:db8:a0b:12f0::1": %v`, err)
	}

	if yes(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestSourceNotErr(t *testing.T) {
	t.Log(`Passing anything else than a number should return an error.`)

	_, err := SourceNot("fail")

	if err == nil {
		t.Error(`SourceNot parsed "fail" as a valid expression, it shouldn't: only an ip address is valid.`)
	}
}
func TestDestinationTrue(t *testing.T) {
	t.Log(`Passing "2001:db8:a0b:12f0::2" as an expression to Destination should match an IPv6 segment having its destination ip field set to "2001:db8:a0b:12f0::2"`)
	yes, err := Destination("2001:db8:a0b:12f0::2")
	if err != nil {
		t.Errorf(`Destination could not parse "2001:db8:a0b:12f0::2": %v`, err)
	}

	if !yes(testPacket) {
		t.Error(`The given packet didn't match.`)
	}
}

func TestDestinationFalse(t *testing.T) {
	t.Log(`Passing "2001:db8:a0b:12f0::1" as an expression to Destination shouldn't match an IPv6 segment having its destination ip field not set to "2001:db8:a0b:12f0::1"`)
	no, err := Destination("2001:db8:a0b:12f0::1")
	if err != nil {
		t.Errorf(`Destination could not parse "2001:db8:a0b:12f0::1": %v`, err)
	}

	if no(testPacket) {
		t.Error(`The given packet didn't match.`)
	}
}

func TestDestinationErr(t *testing.T) {
	t.Log(`Passing anything else than a number should return an error.`)

	_, err := Destination("fail")

	if err == nil {
		t.Error(`Destination parsed "fail" as a valid expression, it shouldn't: only an ip address is valid.`)
	}
}

func TestDestinationNotFalse(t *testing.T) {
	t.Log(`Passing "2001:db8:a0b:12f0::2" as an expression to DestinationNot shouldn't match an IPv6 segment having its destination ip field set to "2001:db8:a0b:12f0::2"`)
	yes, err := DestinationNot("2001:db8:a0b:12f0::2")
	if err != nil {
		t.Errorf(`DestinationNot could not parse "2001:db8:a0b:12f0::2": %v`, err)
	}

	if yes(testPacket) {
		t.Error(`The given packet didn't match.`)
	}
}

func TestDestinationNotTrue(t *testing.T) {
	t.Log(`Passing "2001:db8:a0b:12f0::1" as an expression to DestinationNot should match an IPv6 segment having its destination ip field not set to "2001:db8:a0b:12f0::1"`)
	no, err := DestinationNot("2001:db8:a0b:12f0::1")
	if err != nil {
		t.Errorf(`DestinationNot could not parse "2001:db8:a0b:12f0::1": %v`, err)
	}

	if !no(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestDestinationNotErr(t *testing.T) {
	t.Log(`Passing anything else than a number should return an error.`)

	_, err := DestinationNot("fail")

	if err == nil {
		t.Error(`DestinationNot parsed "fail" as a valid expression, it shouldn't: only an ip address is valid.`)
	}
}

func TestSourceNetworkTrue(t *testing.T) {

	t.Log(`Passing "2001:db8::/32" as an expression to SourceNetwork should match an IPv6 segment having its source ip belonging to  2001:db8::/32`)
	yes, err := SourceNetwork("2001:db8::/32")
	if err != nil {
		t.Errorf(`SourceNetwork could not parse "2001:db8::/32": %v`, err)
	}

	if !yes(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestSourceNetWorkFalse(t *testing.T) {
	t.Log(`Passing "2001:db7::/32" as an expression to SourceNetwork shouldn't match an IPv6 segment having its source ip not belonging to "2001:db7::/32"`)
	no, err := SourceNetwork("2001:db7::/32")
	if err != nil {
		t.Errorf(`SourceNetwork could not parse "2001:db7::/32": %v`, err)
	}

	if no(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestSourceNetworkErr(t *testing.T) {
	t.Log(`Passing anything else than a network address should return an error.`)

	_, err := SourceNetwork("fail")

	if err == nil {
		t.Error(`SourceNetwork parsed "fail" as a valid expression, it shouldn't: only network address is valid.`)
	}

}

func TestSourceNetworkNotTrue(t *testing.T) {
	t.Log(`Passing "2001:db7::/32" as an expression to SourceNetworkNot should match an IPv6 segment having its source ip not belonging "2001:db7::/32"`)
	yes, err := SourceNot("2001:db7::/32")
	if err != nil {
		t.Errorf(`SourceNetworkNot could not parse "2001:db7::/32": %v`, err)
	}

	if !yes(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestSourceNetworkNotFalse(t *testing.T) {
	t.Log(`Passing "2001:db8::/32" as an expression to SourceNetworkNot shouldn't match an IPv6 segment having its source ip belonging to 2001:db8::/32`)
	no, err := SourceNetworkNot("2001:db8::/32")
	if err != nil {
		t.Errorf(`SourceNetworkNot could not parse "2001:db8::/32": %v`, err)
	}

	if no(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestSourceNetworkNotErr(t *testing.T) {
	t.Log(`Passing anything else than a network address should return an error.`)

	_, err := SourceNetworkNot("fail")

	if err == nil {
		t.Error(`SourceNetworkNot parsed "fail" as a valid expression, it shouldn't: only network address is valid.`)
	}
}

func TestDestinationNetworkTrue(t *testing.T) {
	t.Log(`Passing "2001:db8::/32" as an expression to DestinationNetwork should match an IPv6 segment having its destination ip belonging to 2001:db8::/32`)
	yes, err := DestinationNetwork("2001:db8::/32")
	if err != nil {
		t.Errorf(`DestinationNetwork could not parse "2001:db8::/32": %v`, err)
	}

	if !yes(testPacket) {
		t.Error(`The given packet didn't match.`)
	}
}

func TestDestinationNetWorkFalse(t *testing.T) {
	t.Log(`Passing "2001:db7::/32" as an expression to DestinationNetwork shouldn't match an IPv6 segment having its destination ip not belonging to "2001:db7::/32"`)
	no, err := DestinationNetwork("2001:db7::/32")
	if err != nil {
		t.Errorf(`DestinationNetwork could not parse "2001:db7::/32": %v`, err)
	}

	if no(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestDestinationNetworkErr(t *testing.T) {
	t.Log(`Passing anything else than a network address should return an error.`)

	_, err := DestinationNetwork("fail")

	if err == nil {
		t.Error(`DestinationNetwork parsed "fail" as a valid expression, it shouldn't: only network address is valid.`)
	}

}

func TestDestinationNetworkNotTrue(t *testing.T) {
	t.Log(`Passing "2001:db7::/32" as an expression toDestinationNetworkNot should match an IPv6 segment having its destination ip not belonging to "2001:db7::/32"`)
	yes, err := SourceNot("1")
	if err != nil {
		t.Errorf(`DestinationNetworkNot could not parse "2001:db7::/32": %v`, err)
	}

	if !yes(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestDestinationNetworkNotFalse(t *testing.T) {
	t.Log(`Passing "2001:db8::/32" as an expression to DestinationNetworkNot shouldn't match an IPv6 segment having its destination ip belonging to "2001:db8::/32""`)
	no, err := DestinationNetworkNot("2001:db8::/32")
	if err != nil {
		t.Errorf(`DestinationNetworkNot could not parse "2001:db8::/32": %v`, err)
	}

	if no(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestDestinationNetworkNotErr(t *testing.T) {
	t.Log(`Passing anything else than a network address should return an error.`)

	_, err := DestinationNetworkNot("fail")

	if err == nil {
		t.Error(`DestinationNetworkNot parsed "fail" as a valid expression, it shouldn't: only network address is valid.`)
	}
}

func TestLengthMinTrue(t *testing.T) {
	t.Log(`Passing "50" as an expression to LengthMin should match an IPv6 segment having its length field set to 50 or more`)
	f, err := LengthMin("50")
	if err != nil {
		t.Errorf(`LengthMin could not parse "50": %v`, err)
	}

	if !f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestLengthMinFalse(t *testing.T) {
	t.Log(`Passing "600" as an expression to Destiantion shouldn't match an IPv6 segment having its length field not set to 600 or more`)
	f, err := LengthMin("600")
	if err != nil {
		t.Errorf(`LengthMin could not parse "600": %v`, err)
	}

	if f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestLengthMinErr(t *testing.T) {
	t.Log(`Passing anything else than a number should return an error.`)

	_, err := LengthMin("fail")

	if err == nil {
		t.Error(`LengthMin parsed "fail" as a valid expression, it shouldn't: only a number is valid.`)
	}
}

func TestLengthMaxTrue(t *testing.T) {
	t.Log(`Passing "600" as an expression to LengthMax should match an IPv6 segment having its lenght field set to 600 or less`)
	f, err := LengthMax("600")
	if err != nil {
		t.Errorf(`LengthMax could not parse "600": %v`, err)
	}

	if !f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestLengthMaxFalse(t *testing.T) {
	t.Log(`Passing "50" as an expression to LengthMax shouldn't match an IPv6 segment having its length field not set to 50 or less`)
	f, err := LengthMax("50")
	if err != nil {
		t.Errorf(`LengthMax could not parse "50": %v`, err)
	}

	if f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestLengthMaxErr(t *testing.T) {
	t.Log(`Passing anything else than a number should return an error.`)

	_, err := LengthMax("fail")

	if err == nil {
		t.Error(`LengthMax parsed "fail" as a valid expression, it shouldn't: only a number is valid.`)
	}
}


func TestHopLimitMinTrue(t *testing.T) {
	t.Log(`Passing "50" as an expression to HopLimit should match an IPv6 segment having its hopLimit field set to 50 or more`)
	yes, err := HopLimitMin("50")
	if err != nil {
		t.Errorf(`HopLimitMin could not parse "50": %v`, err)
	}

	if !yes(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestHopLimitMinFalse(t *testing.T) {
	t.Log(`Passing "200" as an expression to HopLimit shouldn't match an IPv6 segment having its HopLimmit field not set to 200 or more`)
	no, err := HopLimitMin("200")
	if err != nil {
		t.Errorf(`HopLimitMin could not parse "200": %v`, err)
	}

	if no(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestHopLimitMinErr(t *testing.T) {
	t.Log(`Passing anything else than a number should return an error.`)

	_, err := HopLimitMin("fail")

	if err == nil {
		t.Error(`HopLimitMin parsed "fail" as a valid expression, it shouldn't: only a number is valid.`)
	}
}

func TestHopLimitMaxTrue(t *testing.T) {
	t.Log(`Passing "200" as an expression to HopLimit should match an IPv6 segment having its hopLimit field set to 200 less`)
	yes, err := HopLimitMax("200")
	if err != nil {
		t.Errorf(`HopLimitMax could not parse "200": %v`, err)
	}

	if !yes(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestHopLimitMaxFalse(t *testing.T) {
	t.Log(`Passing "50" as an expression to HopLimit shouldn't match an IPv6 segment having its hopLimit field not set to 50 or less`)
	no, err := HopLimitMax("50")
	if err != nil {
		t.Errorf(`HopLimitMax could not parse "50": %v`, err)
	}

	if no(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestHopLimitMaxErr(t *testing.T) {
	t.Log(`Passing anything else than a number should return an error.`)

	_, err := HopLimitMax("fail")

	if err == nil {
		t.Error(`HopLimitMax parsed "fail" as a valid expression, it shouldn't: only a number is valid.`)
	}
}

func TestLackingIPv6Layer(t *testing.T) {
	t.Log(`Passing a packet not having an UDP datagram should return false for all predicates.`)

	for expr, fn := range map[string]func(string) (func(packet gopacket.Packet) bool, error){
		"2001:db8:a0b:12f0::1":     Source,
		"2001:db8::/32": SourceNetwork,
		"2001:db8:a0b:12f0::2":     Destination,
		"50":       LengthMin,
		"600":     LengthMax,
		"200" :     HopLimitMax,
	} {
		f, err := fn(expr)
		if err != nil {
			t.Error(err)
		}
		if f(testIPv4Packet) {
			t.Error(`Packet shouldn't have matched: the Ip isn't IPv6`)
		}
		if f(testNoIpPacket) {
			t.Error(`Packet shouldn't have matched: there's no Ip layer`)
		}
	}

}
