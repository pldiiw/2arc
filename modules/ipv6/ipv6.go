package ipv6

import (
	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
	"net"
	"strconv"
	"github.com/pkg/errors"
)

//SourceIp checks if packet's source IP is the right one
//expr format ipv6 address : 2001:db8:a0b:12f0::1


var Predicates = map[string]func(expr string) (func(gopacket.Packet) bool, error){
	"Source":           		Source,
	"SourceNot":        		SourceNot,
	"Destination":      		Destination,
	"DestinationNot":   		DestinationNot,
	"SourceNetwork":      		SourceNetwork,
	"SourceNetworkNot":			SourceNetworkNot,
	"DestinationNetwork": 		DestinationNetwork,
	"DestinationNetworkNot":	DestinationNetworkNot,
	"LengthMin":        		LengthMin,
	"LengthMax":        		LengthMax,
	"HopLimitMin":				HopLimitMin,
	"HopLimitMax":				HopLimitMax,
}


//Source matches when an ipv6 packet has its source field corresponding to the ipv6 address given as an expression.
//The expression that this predicate expects is an ipv6 address, like "2001:db8:a0b:12f0::1".
func Source(expr string) (func(gopacket.Packet) bool, error) {

	srcIp := net.ParseIP(expr)

	if srcIp == nil {
		return nil, errors.New("can't parse expr")
	}

	return func(packet gopacket.Packet) bool {
		if ipv6Layer := packet.Layer(layers.LayerTypeIPv6); ipv6Layer == nil {
			return false
		} else {
			return ipv6Layer.(*layers.IPv6).SrcIP.Equal(srcIp)
		}
	}, nil
}

// SourceNot is Source's complement.
func SourceNot(expr string) (func(gopacket.Packet) bool, error) {
	f, err := Source(expr)

	if err != nil {
		return nil, err
	}

	return func(packet gopacket.Packet) bool {
		return !f(packet)
	}, nil
}

//Destination matches when an ipv6 packet has its destination field corresponding to the ipv6 address given as an expression.
//The expression that this predicate expects is an ipv6 address, like "2001:db8:a0b:12f0::1".
func Destination(expr string) (func(gopacket.Packet) bool, error) {

	dstIp := net.ParseIP(expr)

	if dstIp == nil {
		return nil, errors.New("can't parse expr")
	}

	return func(packet gopacket.Packet) bool {
		if ipv6Layer := packet.Layer(layers.LayerTypeIPv6); ipv6Layer == nil {
			return false
		} else {
			return packet.Layer(layers.LayerTypeIPv6).(*layers.IPv6).DstIP.Equal(dstIp)
		}
	}, nil
}

// DestinationNot is Destination's complement.
func DestinationNot(expr string) (func(gopacket.Packet) bool, error) {
	f, err := Destination(expr)

	if err != nil {
		return nil, errors.New("can't parse expr")
	}

	return func(packet gopacket.Packet) bool {
		return !f(packet)
	}, nil

}

//SourceNetwork matches when an ipv6 packet has its source field is include into an ipv6 network given as an expression.
//The expression that this predicate expects is an ipv6 network address, like "2001:db8::/32".
func SourceNetwork(expr string) (func(gopacket.Packet) bool, error) {

	_, srcNet, err := net.ParseCIDR(expr)

	if err != nil {
		return nil, err
	}

	return func(packet gopacket.Packet) bool {
		if ipv6Layer := packet.Layer(layers.LayerTypeIPv6); ipv6Layer == nil {
			return false
		} else {
			return srcNet.Contains(packet.Layer(layers.LayerTypeIPv6).(*layers.IPv6).SrcIP)
		}
	}, nil
}

// SourceNetworkNot is SourceNetwork's complement.
func SourceNetworkNot(expr string) (func(gopacket.Packet) bool, error) {
	f, err := SourceNetwork(expr)

	if err != nil {
		return nil, err
	}

	return func(packet gopacket.Packet) bool {
		return !f(packet)
	}, nil
}

//SourceNetwork matches when an ipv6 packet has its destination field is include into an ipv6 network given as an expression.
//The expression that this predicate expects is an ipv6 network address, like "2001:db8::/32".
func DestinationNetwork(expr string) (func(gopacket.Packet) bool, error) {

	_, dstNet, err := net.ParseCIDR(expr)

	if err != nil {
		return nil, err
	}

	return func(packet gopacket.Packet) bool {
		if ipv6Layer := packet.Layer(layers.LayerTypeIPv6); ipv6Layer == nil {
			return false
		} else {
			return dstNet.Contains(packet.Layer(layers.LayerTypeIPv6).(*layers.IPv6).DstIP)
		}
	}, nil
}

// DestinationNetworkNot is DestinationNetwork's complement.
func DestinationNetworkNot(expr string) (func(gopacket.Packet) bool, error) {
	f, err := DestinationNetwork(expr)

	if err != nil {
		return nil, err
	}

	return func(packet gopacket.Packet) bool {
		return !f(packet)
	}, nil
}

//LengthMin matches when a ipv6 packet has its length field higher than a number given as an expression.
//The expression that this predicate expects is a number representing the minimum length of an ipv6 packet.
func LengthMin(expr string) (func(gopacket.Packet) bool, error) {

	nExpr, err := strconv.ParseUint(expr, 10, 16)

	if err != nil {
		return nil, err
	}

	parsedExpr := uint16(nExpr)

	return func(packet gopacket.Packet) bool {
		return packet.Layer(layers.LayerTypeIPv6).(*layers.IPv6).Length >= parsedExpr
	}, nil
}

//LengthMax matches when a ipv6 packet has its length field lesser than a number given as an expression.
//The expression that this predicate expects is a number representing the maximum length of an ipv6 packet.
func LengthMax(expr string) (func(gopacket.Packet) bool, error) {

	nExpr, err := strconv.ParseUint(expr, 10, 16)

	if err != nil {
		return nil, err
	}

	parsedExpr := uint16(nExpr)

	return func(packet gopacket.Packet) bool {
		return packet.Layer(layers.LayerTypeIPv6).(*layers.IPv6).Length <= parsedExpr
	}, nil
}

//HopLimitMin matches when an ipv6 packet has its HopLimit field higher than a number given as an expression.
//The expression that this predicate expects is a number representing the minimum number of hop of an ipv6 packet.
func HopLimitMin(expr string) (func(gopacket.Packet) bool, error) {

	nExpr, err := strconv.ParseUint(expr, 10, 8)

	if err != nil {
		return nil, err
	}

	parsedExpr := uint8(nExpr)

	return func(packet gopacket.Packet) bool {
		return packet.Layer(layers.LayerTypeIPv6).(*layers.IPv6).HopLimit >= parsedExpr
	}, nil
}

//LengthMax matches when a ipv6 packet has its HopLimit field lesser than a number given as an expression.
//The expression that this predicate expects is a number representing the maximum number of hop of an ipv6 packet.
func HopLimitMax(expr string) (func(gopacket.Packet) bool, error) {

	nExpr, err := strconv.ParseUint(expr, 10, 8)

	if err != nil {
		return nil, err
	}

	parsedExpr := uint8(nExpr)

	return func(packet gopacket.Packet) bool {
		return packet.Layer(layers.LayerTypeIPv6).(*layers.IPv6).HopLimit <= parsedExpr
	}, nil
}
