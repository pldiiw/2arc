package icmp4

import (
	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
	"os"
	"testing"
)

var testPacket gopacket.Packet
var testUDPPacket gopacket.Packet
var testNoTransportPacket gopacket.Packet

func TestMain(m *testing.M) {
	buf := gopacket.NewSerializeBuffer()
	opts := gopacket.SerializeOptions{}

	gopacket.SerializeLayers(buf, opts, &layers.ICMPv4{})
	testPacket = gopacket.NewPacket(buf.Bytes(), layers.LayerTypeICMPv4, gopacket.Default)
	testPacket.Layer(layers.LayerTypeICMPv4).(*layers.ICMPv4).TypeCode = layers.CreateICMPv4TypeCode(4, 0)

	gopacket.SerializeLayers(buf, opts, &layers.UDP{})
	testUDPPacket = gopacket.NewPacket(buf.Bytes(), layers.LayerTypeUDP, gopacket.Default)

	gopacket.SerializeLayers(buf, opts, &layers.Ethernet{}, &layers.IPv4{})
	testNoTransportPacket = gopacket.NewPacket(buf.Bytes(), layers.LayerTypeIPv4, gopacket.Default)

	os.Exit(m.Run())
}

func TestTypeTrue(t *testing.T) {
	t.Log(`Passing "TypeSourceQuench" as an expression to Type should match an ICMPv4 segment having its type field
set to TypeSourceQuench`)
	f, err := Type("TypeSourceQuench")
	if err != nil {
		t.Errorf(`Type could not parse "TypeSourceQuench": %v`, err)
	}

	if !f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestTypeFalse(t *testing.T) {
	t.Log(`Passing "TypeRedirect" as an expression to Type should match an ICMPv4 segment having its type field
set to TypeRedirect`)
	f, err := Type("TypeRedirect")
	if err != nil {
		t.Errorf(`Type could not parse "TypeRedirect": %v`, err)
	}

	if f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestTypeErr(t *testing.T) {
	t.Log(`Passing anything else than list of ICMPv4 type to Type should return an error.`)

	_, err := Type("fail")

	if err == nil {
		t.Error(`Type parsed "fail" as a valid expression, it shouldn't: only an ICMPv4 type is valid.`)
	}
}

func TestTypeNotTrue(t *testing.T) {
	t.Log(`Passing "TypeRedirect" as an expression to TypeNot should match an ICMPv4 segment having its type field not
set to TypeRedirect.`)
	f, err := TypeNot("")
	if err != nil {
		t.Errorf(`TypeNot could not parse "TypeRedirect": %v`, err)
	}

	if !f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestControlNotFalse(t *testing.T) {
	t.Log(`Passing "TypeSourceQuench" as an expression to TypeNot shouldn't match an ICMPv4 segment having its type field
set to TypeSourceQuench`)
	f, err := TypeNot("TypeSourceQuench")
	if err != nil {
		t.Errorf(`TypeNot could not parse "TypeSourceQuench": %v`, err)
	}

	if f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestTypeNotErr(t *testing.T) {
	t.Log(`Passing anything else than list of ICMPv4 type to TypeNot should return an error`)

	_, err := TypeNot("fail")

	if err == nil {
		t.Error(`TypeNot parsed "fail" as a valid expression, it shouldn't: only an ICMPv4 type is valid.`)
	}
}

func TestLackingICMPv4Layer(t *testing.T) {
	t.Log(`Passing a packet not having an ICMPv4 datagram should return false for all predicates.`)

	for expr, fn := range map[string]func(string) (func(packet gopacket.Packet) bool, error){
		"TypeSourceQuench": Type,
	} {
		f, err := fn(expr)
		if err != nil {
			t.Error(err)
		}
		if f(testUDPPacket) {
			t.Error(`Packet shouldn't have matched: the transport isn't ICMPv4`)
		}
		if f(testNoTransportPacket) {
			t.Error(`Packet shouldn't have matched: there's no transport layer`)
		}
	}

}
