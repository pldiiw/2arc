package icmp4

import (
	"errors"
	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
)

var Predicates = map[string]func(expr string) (func(gopacket.Packet) bool, error){
	"Type":    Type,
	"TypeNot": TypeNot,
}

var icmpv4types = map[string]uint8{
	"EchoReply":                   0,
	"TypeDestinationUnreachable ": 3,
	"TypeSourceQuench":            4,
	"TypeRedirect":                5,
	"TypeEchoRequest":             8,
	"RouterAdvertisement":         9,
	"RouterSolicitation":          10,
	"TimeExceeded":                11,
	"ParameterProblem":            12,
	"TimestampRequest":            13,
	"TimestampReply":              14,
	"InfoRequest":                 15,
	"InfoReply":                   16,
	"AddressMaskRequest":          17,
	"AddressMaskReply":            18,
}

// Type matches when an ICMPv4 packet has its type field corresponding to one of the types given as an expression.
// The expression that this predicate expects is a comma-separated list of one or more ICMPv4 types.
// Example: EchoReply,InfoReply,TimeExceeded
// Here's a list of all the supported types:
// - EchoReply
// - TypeDestinationUnreachable
// - TypeSourceQuench
// - TypeRedirect
// - TypeEchoRequest
// - RouterAdvertisement
// - RouterSolicitation
// - TimeExceeded
// - ParameterProblem
// - TimestampRequest
// - TimestampReply
// - InfoRequest
// - InfoReply
// - AddressMaskRequest
// - AddressMaskReply
func Type(expr string) (func(gopacket.Packet) bool, error) {
	var exprTypes []string
	for len(expr) > 0 {
		for i := range expr {
			if expr[i] == ',' {
				exprTypes = append(exprTypes, expr[:i])
				expr = expr[i+1:]
				break
			}
			if i == len(expr)-1 {
				exprTypes = append(exprTypes, expr[:i+1])
				expr = expr[i+1:]
				break
			}
		}
	}
	for _, icmpv4type := range exprTypes {
		if _, ok := icmpv4types[icmpv4type]; !ok {
			return nil, errors.New("Given ICMPv4 types does not exist: " + icmpv4type)
		}
	}
	return (func(packet gopacket.Packet) bool {
		if icmpc4Layer := packet.Layer(layers.LayerTypeICMPv4); icmpc4Layer == nil {
			return false
		} else {
			for _, i := range exprTypes {
				if icmpv4types[i] == icmpc4Layer.(*layers.ICMPv4).TypeCode.Type() {
					return true
				}
			}
			return false
		}
	}), nil
}

// TypeNot is Type's complement.
func TypeNot(expr string) (func(gopacket.Packet) bool, error) {
	f, err := Type(expr)
	if err != nil {
		return nil, err
	}

	return (func(packet gopacket.Packet) bool {
		return !f(packet)
	}), nil
}
