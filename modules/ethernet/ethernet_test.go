package ethernet

import (
	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
	"net"
	"os"
	"testing"
)

var testPacket gopacket.Packet
var testIPv6Packet gopacket.Packet
var testNoEthernetPacket gopacket.Packet

func TestMain(m *testing.M) {


	srcMac, _ := net.ParseMAC("00:DC:76:D9:76:A1")
	dstMac, _ := net.ParseMAC("00-1E-33-1D-6A-79")

	buf := gopacket.NewSerializeBuffer()
	opts := gopacket.SerializeOptions{}
	gopacket.SerializeLayers(buf, opts, &layers.Ethernet{
		SrcMAC:    srcMac,
		DstMAC:    dstMac,
		EthernetType :    layers.EthernetTypeEthernetCTP,

	})

	testPacket = gopacket.NewPacket(buf.Bytes(), layers.LayerTypeEthernet, gopacket.Default)

	gopacket.SerializeLayers(buf, opts, &layers.IPv6{})
	testIPv6Packet = gopacket.NewPacket(buf.Bytes(), layers.LayerTypeIPv6, gopacket.Default)

	gopacket.SerializeLayers(buf, opts, &layers.IPv4{}, &layers.TCP{})
	testNoEthernetPacket = gopacket.NewPacket(buf.Bytes(), layers.LayerTypeTCP, gopacket.Default)
	os.Exit(m.Run())
}


func TestSourceTrue(t *testing.T) {
	t.Log(`Passing "00:DC:76:D9:76:A1" as an expression to Source should match an Ethernet segment having its source mac field set to "00:DC:76:D9:76:A1"`)
	f, err := Source("00:DC:76:D9:76:A1")
	if err != nil {
		t.Errorf(`Source could not parse "00:DC:76:D9:76:A1": %v`, err)
	}

	if !f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestSourceFalse(t *testing.T) {
	t.Log(`Passing "00-1E-33-1D-6A-79" as an expression to Source shouldn't match an mac segment having its source mac field not set to "00-1E-33-1D-6A-79"`)
	no, err := Source("00-1E-33-1D-6A-79")
	if err != nil {
		t.Errorf(`Source could not parse "00-1E-33-1D-6A-79": %v`, err)
	}

	if no(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestSourceErr(t *testing.T) {
	t.Log(`Passing anything else than a number should return an error.`)

	_, err := Source("fail")

	if err == nil {
		t.Error(`Source parsed "fail" as a valid expression, it shouldn't: only an mac address is valid.`)
	}
}

func TestSouceNotTrue(t *testing.T) {

	t.Log(`Passing "00-1E-33-1D-6A-79"as an expression to SourceNot should match an mac segment having its source mac field not set to "00-1E-33-1D-6A-79"`)
	yes, err := SourceNot("00-1E-33-1D-6A-79")
	if err != nil {
		t.Errorf(`SourceNot could not parse "00-1E-33-1D-6A-79": %v`, err)
	}

	if !yes(testPacket) {
		t.Error(`The given packet didn't match.`)
	}
}

func TestSourceNotFalse(t *testing.T) {

	t.Log(`Passing "00:DC:76:D9:76:A1" as an expression to SourceNot shouldn't match an mac segment having its source mac field set to "00:DC:76:D9:76:A1"`)
	yes, err := SourceNot("00:DC:76:D9:76:A1")
	if err != nil {
		t.Errorf(`SourceNot could not parse "00:DC:76:D9:76:A1": %v`, err)
	}

	if yes(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestSourceNotErr(t *testing.T) {
	t.Log(`Passing anything else than a number should return an error.`)

	_, err := SourceNot("fail")

	if err == nil {
		t.Error(`SourceNot parsed "fail" as a valid expression, it shouldn't: only an mac address is valid.`)
	}
}

func TestDestinationTrue(t *testing.T) {
	t.Log(`Passing "00-1E-33-1D-6A-79" as an expression to Destination should match an mac segment having its destination mac field set to "00-1E-33-1D-6A-79"`)
	yes, err := Destination("00-1E-33-1D-6A-79")
	if err != nil {
		t.Errorf(`Destination could not parse "00-1E-33-1D-6A-79": %v`, err)
	}

	if !yes(testPacket) {
		t.Error(`The given packet didn't match.`)
	}
}

func TestDestinationFalse(t *testing.T) {
	t.Log(`Passing "00:DC:76:D9:76:A1" as an expression to Destination shouldn't match an mac segment having its destination mac field not set to "00:DC:76:D9:76:A1"`)
	no, err := Destination("00:DC:76:D9:76:A1")
	if err != nil {
		t.Errorf(`Destination could not parse "00:DC:76:D9:76:A1": %v`, err)
	}

	if no(testPacket) {
		t.Error(`The given packet didn't match.`)
	}
}

func TestDestinationErr(t *testing.T) {
	t.Log(`Passing anything else than a number should return an error.`)

	_, err := Destination("fail")

	if err == nil {
		t.Error(`Destination parsed "fail" as a valid expression, it shouldn't: only an mac address is valid.`)
	}
}

func TestDestinationNotFalse(t *testing.T) {
	t.Log(`Passing "00-1E-33-1D-6A-79" as an expression to DestinationNot shouldn't match an mac segment having its destination mac field set to "00-1E-33-1D-6A-79"`)
	yes, err := DestinationNot("00-1E-33-1D-6A-79")
	if err != nil {
		t.Errorf(`DestinationNot could not parse "00-1E-33-1D-6A-79": %v`, err)
	}

	if yes(testPacket) {
		t.Error(`The given packet didn't match.`)
	}
}

func TestDestinationNotTrue(t *testing.T) {
	t.Log(`Passing "00:DC:76:D9:76:A1" as an expression to DestinationNot should match an mac segment having its destination mac field not set to "00:DC:76:D9:76:A1"`)
	no, err := DestinationNot("00:DC:76:D9:76:A1")
	if err != nil {
		t.Errorf(`DestinationNot could not parse "00:DC:76:D9:76:A1": %v`, err)
	}

	if !no(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestDestinationNotErr(t *testing.T) {
	t.Log(`Passing anything else than a number should return an error.`)

	_, err := DestinationNot("fail")

	if err == nil {
		t.Error(`DestinationNot parsed "fail" as a valid expression, it shouldn't: only an mac address is valid.`)
	}
}


func TestEtherTypeTrue(t *testing.T) {
	t.Log(`Passing "EthernetTypeIPv4,EthernetTypeTransparentEthernetBridging,EthernetTypeEthernetCTP" as an expression to EtherType should match an Ethernet segment having its EthernetType field set on one of them`)
	f, err := EtherType("EthernetTypeIPv4,EthernetTypeTransparentEthernetBridging,EthernetTypeEthernetCTP")
	if err != nil {
		t.Errorf(`EtherType could not parse "EthernetTypeIPv4,EthernetTypeTransparentEthernetBridging,EthernetTypeEthernetCTP": %v`, err)
	}

	if !f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestEtherTypeFalse(t *testing.T) {
	t.Log(`Passing "EthernetTypeARP,EthernetTypeMPLSMulticast,EthernetTypeDot1Q" as an expression to EtherType shouldn't match an Ethernet segment having its EthernetType field not set on one of them`)
	f, err := EtherType("EthernetTypeARP,EthernetTypeMPLSMulticast,EthernetTypeDot1Q")
	if err != nil {
		t.Errorf(`EtherType could not parse "EthernetTypeARP,EthernetTypeMPLSMulticast,EthernetTypeDot1Q": %v`, err)
	}

	if f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestEtherTypeErr(t *testing.T) {
	t.Log(`Passing anything else than strings separated by "," should return an error.`)

	_, err := EtherType("fail")

	if err == nil {
		t.Error(`EtherType parsed "fail" as a valid expression, it shouldn't: only strings are valid.`)
	}

}
