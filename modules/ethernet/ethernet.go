package ethernet

import (
	"bytes"
	"errors"
	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
	"net"
	"strings"
)



var Predicates = map[string]func(expr string) (func(gopacket.Packet) bool, error){
	"Source":           		Source,
	"SourceNot":        		SourceNot,
	"Destination":      		Destination,
	"DestinationNot":   		DestinationNot,
	"EtherType":				EtherType,
}

var ethertype = map[string]layers.EthernetType{

	"EthernetTypeLLC":                         layers.EthernetTypeLLC,
	"EthernetTypeIPv4":                        layers.EthernetTypeIPv4,
	"EthernetTypeARP":                         layers.EthernetTypeARP,
	"EthernetTypeIPv6":                        layers.EthernetTypeIPv6,
	"EthernetTypeCiscoDiscovery":              layers.EthernetTypeCiscoDiscovery,
	"EthernetTypeNortelDiscovery":             layers.EthernetTypeNortelDiscovery,
	"EthernetTypeTransparentEthernetBridging": layers.EthernetTypeTransparentEthernetBridging,
	"EthernetTypeDot1Q":                       layers.EthernetTypeDot1Q,
	"EthernetTypePPPoEDiscovery":              layers.EthernetTypePPPoEDiscovery,
	"EthernetTypePPPoESession":                layers.EthernetTypePPPoESession,
	"EthernetTypeMPLSUnicast":                 layers.EthernetTypeMPLSUnicast,
	"EthernetTypeMPLSMulticast":               layers.EthernetTypeMPLSMulticast,
	"EthernetTypeEAPOL":                       layers.EthernetTypeEAPOL,
	"EthernetTypeQinQ":                        layers.EthernetTypeQinQ,
	"EthernetTypeLinkLayerDiscovery":          layers.EthernetTypeLinkLayerDiscovery,
	"EthernetTypeEthernetCTP":                 layers.EthernetTypeEthernetCTP,
}

func stringToArray(expr string) []string {
	var newArray []string

	for len(expr) > 0 {
		for i := range expr {
			if expr[i] == ',' {
				newArray = append(newArray, expr[:i])
				expr = expr[i+1:]
				break
			}
			if i == len(expr)-1 {
				newArray = append(newArray, expr[:i+1])
				expr = expr[i+1:]
				break
			}
		}

	}

	return newArray
}

//EtherType matchs when an ethernet packet has its EthernetType field corresponding to one of the values given as an expression
//The expression that this predicate exoects an Ethertype layer, expr egg : "IPProtocolTCP,IPProtocolEtherIP,IPProtocolNoNextHeader"
func EtherType(expr string) (func(gopacket.Packet) bool, error) {

	if strings.Count(expr, ",") == 0 {
		return nil, errors.New(" no ',' found")
	}

	typeToCheck := stringToArray(expr)

	var ethernetType []layers.EthernetType
	for i := 0; i < len(typeToCheck); i++ {
		ethernetType = append(ethernetType, ethertype[typeToCheck[i]])
	}

	return func(packet gopacket.Packet) bool {
		if ethernetLayer := packet.Layer(layers.LayerTypeEthernet); ethernetLayer == nil {
			return false
		} else {
			result := false
			for _, b := range ethernetType {
				if packet.Layer(layers.LayerTypeEthernet).(*layers.Ethernet).EthernetType == b {
					result = true
				}
			}
			return result
		}
	}, nil

}


//Source match if packet's source Mac is the same as the Ethernet packet
//The expression that this predicate expects is a string with format 01:23:45:67:89:ab:cd:ef
func Source(expr string) (func(gopacket.Packet) bool, error) {

	srcMac, err := net.ParseMAC(expr)

	if err != nil {
		return nil, err
	}

	return func(packet gopacket.Packet) bool {

		if ethernetLayer := packet.Layer(layers.LayerTypeEthernet); ethernetLayer == nil {
			return false
		} else {
			return bytes.Equal(packet.Layer(layers.LayerTypeEthernet).(*layers.Ethernet).SrcMAC, srcMac)
		}
	}, nil

}

//SourceNot is Source not
func SourceNot(expr string) (func(gopacket.Packet) bool, error) {
	f, err := Source(expr)

	if err != nil {
		return nil, err
	}

	return func(packet gopacket.Packet) bool {
		return !f(packet)
	}, nil
}


//Destination match if packet's destination IP is the same as the packet Destination IP packet
//The expression that this predicate expects is a string with format 01:23:45:67:89:ab:cd:ef
func Destination(expr string) (func(gopacket.Packet) bool, error) {

	nExpr, err := net.ParseMAC(expr)

	if err != nil {
		return nil, err
	}

	return func(packet gopacket.Packet) bool {
		if ethernetLayer := packet.Layer(layers.LayerTypeEthernet); ethernetLayer == nil {
			return false
		} else {
			return bytes.Equal(packet.Layer(layers.LayerTypeEthernet).(*layers.Ethernet).DstMAC, nExpr)
		}
	}, nil
}

//DestinationNot is Destination Not
func DestinationNot(expr string) (func(gopacket.Packet) bool, error) {
	f, err := Destination(expr)

	if err != nil {
		return nil, err
	}

	return func(packet gopacket.Packet) bool {
		return !f(packet)
	}, nil
}
