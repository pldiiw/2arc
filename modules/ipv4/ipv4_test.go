package ipv4

import (
	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
	"net"
	"os"
	"strconv"
	"testing"
)

var testPacket gopacket.Packet
var testIPv6Packet gopacket.Packet
var testNoIpPacket gopacket.Packet

func TestMain(m *testing.M) {

	nExpr, _ := strconv.ParseUint("111", 10, 8)

	ttl, _ := strconv.ParseUint("100", 10, 8)

	length, _ := strconv.ParseUint("300", 10, 16)

	buf := gopacket.NewSerializeBuffer()
	opts := gopacket.SerializeOptions{}
	gopacket.SerializeLayers(buf, opts, &layers.IPv4{
		SrcIP:    net.ParseIP("192.168.1.1"),
		DstIP:    net.ParseIP("192.168.1.2"),
		Flags:    layers.IPv4EvilBit ,
		Protocol: layers.IPProtocolIPv4,
		TOS:      uint8(nExpr),
		TTL:      uint8(ttl),
		Length:   uint16(length),
	})

	testPacket = gopacket.NewPacket(buf.Bytes(), layers.LayerTypeIPv4, gopacket.Default)

	gopacket.SerializeLayers(buf, opts, &layers.IPv6{})
	testIPv6Packet = gopacket.NewPacket(buf.Bytes(), layers.LayerTypeIPv6, gopacket.Default)

	gopacket.SerializeLayers(buf, opts, &layers.Ethernet{}, &layers.TCP{})
	testNoIpPacket = gopacket.NewPacket(buf.Bytes(), layers.LayerTypeTCP, gopacket.Default)

	os.Exit(m.Run())
}

func TestSourceTrue(t *testing.T) {
	t.Log(`Passing "192.168.1.1" as an expression to Source should match an IPv4 segment having its source ip field set to "192.168.1.1"`)
	f, err := Source("192.168.1.1")
	if err != nil {
		t.Errorf(`Source could not parse "192.168.1.1": %v`, err)
	}

	if !f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestSourceFalse(t *testing.T) {
	t.Log(`Passing "192.168.1.2" as an expression to Source shouldn't match an IPv4 segment having its source ip field not set to "192.168.1.2"`)
	f, err := Source("192.168.1.2")
	if err != nil {
		t.Errorf(`Source could not parse "192.168.1.2": %v`, err)
	}

	if f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestSourceErr(t *testing.T) {
	t.Log(`Passing anything else than a number should return an error.`)

	_, err := Source("fail")

	if err == nil {
		t.Error(`Source parsed "fail" as a valid expression, it shouldn't: only an ip address is valid.`)
	}
}

func TestSouceNotTrue(t *testing.T) {

	t.Log(`Passing "192.168.1.2" as an expression to SourceNot should match an IPv4 segment having its source ip field not set to "192.168.1.2"`)
	f, err := SourceNot("192.168.1.2")
	if err != nil {
		t.Errorf(`SourceNot could not parse "192.168.1.2": %v`, err)
	}

	if !f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}
}

func TestSourceNotFalse(t *testing.T) {

	t.Log(`Passing "192.168.1.1" as an expression to SourceNot shouldn't match an IPv4 segment having its source ip field set to "192.168.1.1"`)
	f, err := SourceNot("192.168.1.1")
	if err != nil {
		t.Errorf(`SourceNot could not parse "192.168.1.1": %v`, err)
	}

	if f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestSourceNotErr(t *testing.T) {
	t.Log(`Passing anything else than a number should return an error.`)

	_, err := SourceNot("fail")

	if err == nil {
		t.Error(`SourceNot parsed "fail" as a valid expression, it shouldn't: only an ip address is valid.`)
	}
}

func TestDestinationTrue(t *testing.T) {
	t.Log(`Passing "192.168.1.2" as an expression to Destination should match an IPv4 segment having its destination ip field set to "192.168.1.2"`)
	f, err := Destination("192.168.1.2")
	if err != nil {
		t.Errorf(`Destination could not parse "192.168.1.2": %v`, err)
	}

	if !f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}
}

func TestDestinationFalse(t *testing.T) {
	t.Log(`Passing "192.168.1.1" as an expression to Destination shouldn't match an IPv4 segment having its destination ip field not set to "192.168.1.1"`)
	f, err := Destination("192.168.1.1")
	if err != nil {
		t.Errorf(`Destination could not parse "192.168.1.1": %v`, err)
	}

	if f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}
}

func TestDestinationErr(t *testing.T) {
	t.Log(`Passing anything else than a number should return an error.`)

	_, err := Destination("fail")

	if err == nil {
		t.Error(`Destination parsed "fail" as a valid expression, it shouldn't: only an ip address is valid.`)
	}
}

func TestDestinationNotFalse(t *testing.T) {
	t.Log(`Passing "192.168.1.2" as an expression to DestinationNot shouldn't match an IPv4 segment having its destination ip field set to "192.168.1.2"`)
	f, err := DestinationNot("192.168.1.2")
	if err != nil {
		t.Errorf(`DestinationNot could not parse "192.168.1.2": %v`, err)
	}

	if f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}
}

func TestDestinationNotTrue(t *testing.T) {
	t.Log(`Passing "192.168.1.1" as an expression to DestinationNot should match an IPv4 segment having its destination ip field not set to "192.168.1.1"`)
	f, err := DestinationNot("192.168.1.1")
	if err != nil {
		t.Errorf(`DestinationNot could not parse "192.168.1.1": %v`, err)
	}

	if !f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestDestinationNotErr(t *testing.T) {
	t.Log(`Passing anything else than a number should return an error.`)

	_, err := DestinationNot("fail")

	if err == nil {
		t.Error(`DestinationNot parsed "fail" as a valid expression, it shouldn't: only an ip address is valid.`)
	}
}

func TestSourceNetworkTrue(t *testing.T) {

	t.Log(`Passing "192.168.1.0/24" as an expression to SourceNetwork should match an IPv4 segment having its source ip belonging to in  "192.168.1.0/24"`)
	f, err := SourceNetwork("192.168.1.0/24")
	if err != nil {
		t.Errorf(`SourceNetwork could not parse "192.168.1.0/24": %v`, err)
	}

	if !f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestSourceNetWorkFalse(t *testing.T) {
	t.Log(`Passing "192.168.2.0/24" as an expression to SourceNetwork shouldn't match an IPv4 segment having its source ip not belonging to in "192.168.2.0/24"`)
	f, err := SourceNetwork("192.168.2.0/24")
	if err != nil {
		t.Errorf(`SourceNetwork could not parse "1-2": %v`, err)
	}

	if f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestSourceNetworkErr(t *testing.T) {
	t.Log(`Passing anything else than a network address should return an error.`)

	_, err := SourceNetwork("fail")

	if err == nil {
		t.Error(`The given packet didn't match.`)
	}

}

func TestSourceNetworkNotTrue(t *testing.T) {
	t.Log(`Passing "192.168.2.0/24" as an expression to SourceNetworkNot should match an IPv4 segment having its source ip not belonging to "192.168.2.0/24"`)
	f, err := SourceNetworkNot("192.168.2.0/24")
	if err != nil {
		t.Errorf(`SourceNetworkNot could not parse "192.168.2.0/24": %v`, err)
	}

	if !f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestSourceNetworkNotFalse(t *testing.T) {
	t.Log(`Passing "192.168.1.0/24" as an expression to SourceNetworkNot shouldn't match an IPv4 segment having its source ip belonging to "192.168.1.0/24"`)
	f, err := SourceNetworkNot("192.168.1.0/24")
	if err != nil {
		t.Errorf(`SourceNetworkNot could not parse "192.168.1.0/24": %v`, err)
	}

	if f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestSourceNetworkNotErr(t *testing.T) {
	t.Log(`Passing anything else than a network address should return an error.`)

	_, err := SourceNetworkNot("fail")

	if err == nil {
		t.Error(`SourceNetworkNot parsed "fail" as a valid expression, it shouldn't: only network address is valid.`)
	}
}

func TestDestinationNetworkTrue(t *testing.T) {
	t.Log(`Passing "192.168.1.0/24" as an expression to DestinationNetwork should match an IPv4 segment having its destination ip belonging to in "192.168.1.0/24"`)
	f, err := DestinationNetwork("192.168.1.0/24")
	if err != nil {
		t.Errorf(`DestinationNetwork could not parse "192.168.1.0/24": %v`, err)
	}

	if !f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}
}

func TestDestinationNetWorkFalse(t *testing.T) {
	t.Log(`Passing "192.168.2.0/24" as an expression to DestinationNetwork shouldn't match an IPv4 segment having its destination ip not belonging to in "192.168.2.0/24"`)
	f, err := DestinationNetwork("192.168.2.0/24")
	if err != nil {
		t.Errorf(`DestinationNetwork could not parse "1-2": %v`, err)
	}

	if f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestDestinationNetworkErr(t *testing.T) {
	t.Log(`Passing anything else than a network address should return an error.`)

	_, err := DestinationNetwork("fail")

	if err == nil {
		t.Error(`DestinationNetwork parsed "fail" as a valid expression, it shouldn't: only network address is valid.`)
	}

}

func TestDestinationNetworkNotTrue(t *testing.T) {
	t.Log(`Passing "192.168.2.0/24" as an expression toDestinationNetworkNot should match an IPv4 segment having its destination ip not belonging to "192.168.2.0/24"`)
	f, err := DestinationNetworkNot("192.168.2.0/24")
	if err != nil {
		t.Errorf(`DestinationNetworkNot could not parse "192.168.2.0/24": %v`, err)
	}

	if !f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestDestinationNetworkNotFalse(t *testing.T) {
	t.Log(`Passing "192.168.1.0/24" as an expression to DestinationNetworkNot shouldn't match an IPv4 segment having its destination ip belonging to "192.168.1.0/24"`)
	f, err := DestinationNetworkNot("192.168.1.0/24")
	if err != nil {
		t.Errorf(`DestinationNetworkNot could not parse "192.168.1.0/24": %v`, err)
	}

	if f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestDestinationNetworkNotErr(t *testing.T) {
	t.Log(`Passing anything else than a network address should return an error.`)

	_, err := DestinationNetworkNot("fail")

	if err == nil {
		t.Error(`DestinationNetworkNot parsed "fail" as a valid expression, it shouldn't: only network address is valid.`)
	}
}

func TestProtocolTrue(t *testing.T) {
	t.Log(`Passing "IPProtocolIPv4,IPProtocolEtherIP,IPProtocolNoNextHeader" as an expression to Protocol should match an IPv4 segment having its Protocol field set on one of them`)
	f, err := Protocol("IPProtocolIPv4,IPProtocolEtherIP,IPProtocolNoNextHeader")
	if err != nil {
		t.Errorf(`Protocol could not parse "IPProtocolIPv4,IPProtocolEtherIP,IPProtocolNoNextHeader": %v`, err)
	}

	if !f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestProtocolFalse(t *testing.T) {
	t.Log(`Passing "IPProtocolICMPv4,IPProtocolIPv6,IPProtocolSCTP" as an expression to Protocol shouldn't match an IPv4 segment having its Protocol field not set on one of them`)
	f, err := Protocol("IPProtocolICMPv4,IPProtocolIPv6,IPProtocolSCTP")
	if err != nil {
		t.Errorf(`Protocol could not parse "IPProtocolICMPv4,IPProtocolIPv4,IPProtocolSCTP": %v`, err)
	}

	if f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestProtocolErr(t *testing.T) {
	t.Log(`Passing anything else than strings separated by "," should return an error.`)

	_, err := Protocol("fail")

	if err == nil {
		t.Error(`Protocol parsed "fail" as a valid expression, it shouldn't: only strings are valid.`)
	}

}

func TestLengthMinTrue(t *testing.T) {
	t.Log(`Passing "50" as an expression to LengthMin should match an IPv4 segment having its length field set to 50 or more`)
	f, err := LengthMin("50")
	if err != nil {
		t.Errorf(`LengthMin could not parse "50": %v`, err)
	}

	if !f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestLengthMinFalse(t *testing.T) {
	t.Log(`Passing "600" as an expression to Destiantion shouldn't match an IPv4 segment having its length field not set to 600 or more`)
	f, err := LengthMin("600")
	if err != nil {
		t.Errorf(`LengthMin could not parse "600": %v`, err)
	}

	if f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestLengthMinErr(t *testing.T) {
	t.Log(`Passing anything else than a number should return an error.`)

	_, err := LengthMin("fail")

	if err == nil {
		t.Error(`LengthMin parsed "fail" as a valid expression, it shouldn't: only a number is valid.`)
	}
}

func TestLengthMaxTrue(t *testing.T) {
	t.Log(`Passing "600" as an expression to LengthMax should match an IPv4 segment having its lenght field set to 600 or less`)
	f, err := LengthMax("600")
	if err != nil {
		t.Errorf(`LengthMax could not parse "600": %v`, err)
	}

	if !f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestLengthMaxFalse(t *testing.T) {
	t.Log(`Passing "50" as an expression to LengthMax shouldn't match an IPv4 segment having its length field not set to 50 or less`)
	f, err := LengthMax("50")
	if err != nil {
		t.Errorf(`LengthMax could not parse "50": %v`, err)
	}

	if f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestLengthMaxErr(t *testing.T) {
	t.Log(`Passing anything else than a number should return an error.`)

	_, err := LengthMax("fail")

	if err == nil {
		t.Error(`LengthMax parsed "fail" as a valid expression, it shouldn't: only a number is valid.`)
	}
}

func TestTosTrue(t *testing.T) {
	t.Log(`Passing "10110011" as an expression to Tos should match an IPv4 segment having its TOS field set to "10110011"`)
	f, err := LengthMax("10110011")
	if err != nil {
		t.Errorf(`Tos could not parse "10110011": %v`, err)
	}

	if !f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestTosFalse(t *testing.T) {
	t.Log(`Passing "1101111"  as an expression to Tos shouldn't match an IPv4 segment having its TOS field not set to "1101111"`)
	f, err := LengthMax("1101111")
	if err != nil {
		t.Errorf(`Tos could not parse "1101111": %v`, err)
	}

	if f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestTosErr(t *testing.T) {
	t.Log(`Passing anything else than a number should return an error.`)

	_, err := LengthMax("fail")

	if err == nil {
		t.Error(`Tos parsed "fail" as a valid expression, it shouldn't: only a number is valid.`)
	}
}

func TestFlagTrue(t *testing.T) {
	t.Log(`Passing "IPv4EvilBit,IPv4DontFragment" as an expression to Flag should match an IPv4 segment having its Flag bits fields set on one of them`)
	f, err := Flag("IPv4EvilBit,IPv4DontFragment")
	if err != nil {
		t.Errorf(`Flag could not parse "IPv4EvilBit,IPv4DontFragment": %v`, err)
	}

	if !f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestFlagFalse(t *testing.T) {
	t.Log(`Passing "IPv4DontFragment,IPv4MoreFragments" as an expression to Flag shouldn't match an IPv4 segment having its Flag bits field not set on one of them`)
	f, err := Flag("IPv4DontFragment,IPv4MoreFragments")
	if err != nil {
		t.Errorf(`Flag could not parse "IPv4DontFragment,IPv4MoreFragments": %v`, err)
	}

	if f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestFlagErr(t *testing.T) {
	t.Log(`Passing anything else than strings separated by "," should return an error.`)

	_, err := Flag("fail")

	if err == nil {
		t.Error(`Flag parsed "fail" as a valid expression, it shouldn't: only strings are valid.`)
	}

}

func TestTtlMaxTrue(t *testing.T) {
	t.Log(`Passing "200" as an expression to TtlMax should match an IPv4 segment having its TTL field set to 200 less`)
	f, err := TtlMax("200")
	if err != nil {
		t.Errorf(`TtlMax could not parse "200": %v`, err)
	}

	if !f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestTtlMaxFalse(t *testing.T) {
	t.Log(`Passing "50"  as an expression to TtlMax shouldn't match an IPv4 segment having its TTL field not set to 50 or less`)
	f, err := TtlMax("50")
	if err != nil {
		t.Errorf(`TtlMax could not parse "50": %v`, err)
	}

	if f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestTtlMaxErr(t *testing.T) {
	t.Log(`Passing anything else than a number should return an error.`)

	_, err := TtlMax("fail")

	if err == nil {
		t.Error(`TtlMax parsed "fail" as a valid expression, it shouldn't: only a number is valid.`)
	}
}

func TestTtlMinTrue(t *testing.T) {
	t.Log(`Passing "50" as an expression to TtlMin should match an IPv4 segment having its TTL field set to 50 or more`)
	f, err := TtlMin("50")
	if err != nil {
		t.Errorf(`TtlMin could not parse "50": %v`, err)
	}

	if !f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestTtlMinFalse(t *testing.T) {
	t.Log(`Passing "200" as an expression to TtlMin shouldn't match an IPv4 segment having its length field not set to 200 or more`)
	f, err := TtlMin("200")
	if err != nil {
		t.Errorf(`TtlMin could not parse "200": %v`, err)
	}

	if f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestTtlMinErr(t *testing.T) {
	t.Log(`Passing anything else than a number should return an error.`)

	_, err := TtlMin("fail")

	if err == nil {
		t.Error(`TtlMin parsed "fail" as a valid expression, it shouldn't: only a number is valid.`)
	}
}

func TestLackingIPv6Layer(t *testing.T) {
	t.Log(`Passing a packet not having an UDP datagram should return false for all predicates.`)

	for expr, fn := range map[string]func(string) (func(packet gopacket.Packet) bool, error){
		"192.168.1.1":     Source,
		"2192.168.1.0/24": SourceNetwork,
		"192.168.1.2":     Destination,
		"50":       LengthMin,
		"600":     LengthMax,
		"200" :     TtlMax,
		"IPv4DontFragment,IPv4MoreFragments" : Flag,
		"IPProtocolIPv4,IPProtocolEtherIP,IPProtocolNoNextHeader" : Protocol,
	} {
		f, err := fn(expr)
		if err != nil {
			t.Error(err)
		}
		if f(testIPv6Packet) {
			t.Error(`Packet shouldn't have matched: the Ip isn't IPv4`)
		}
		if f(testNoIpPacket) {
			t.Error(`Packet shouldn't have matched: there's no Ip layer`)
		}
	}

}