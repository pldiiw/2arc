package ipv4

import (
	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
	"github.com/pkg/errors"
	"net"
	"strconv"
	"strings"
)

var Predicates = map[string]func(expr string) (func(gopacket.Packet) bool, error){
	"Source":           		Source,
	"SourceNot":        		SourceNot,
	"Destination":      		Destination,
	"DestinationNot":   		DestinationNot,
	"SourceNetwork":      		SourceNetwork,
	"SourceNetworkNot":			SourceNetworkNot,
	"DestinationNetwork": 		DestinationNetwork,
	"DestinationNetworkNot":	DestinationNetworkNot,
	"Tos":						Tos,
	"Flag":						Flag,
	"Protocol":					Protocol,
	"LengthMin":        		LengthMin,
	"LengthMax":        		LengthMax,
	"TtlMin":					TtlMin,
	"TtlMax":					TtlMax,
}

var listofFlag = map[string]layers.IPv4Flag{
	"IPv4EvilBit":       layers.IPv4EvilBit,
	"IPv4DontFragment":  layers.IPv4DontFragment,
	"IPv4MoreFragments": layers.IPv4MoreFragments}

var listofProtocol = map[string]layers.IPProtocol{
	"IPProtocolIPv6HopByHop":    layers.IPProtocolIPv6HopByHop,
	"IPProtocolICMPv4":          layers.IPProtocolICMPv4,
	"IPProtocolIGMP":            layers.IPProtocolIGMP,
	"IPProtocolIPv4":            layers.IPProtocolIPv4,
	"IPProtocolTCP":             layers.IPProtocolTCP,
	"IPProtocolUDP":             layers.IPProtocolUDP,
	"IPProtocolRUDP":            layers.IPProtocolRUDP,
	"IPProtocolIPv6":            layers.IPProtocolIPv6,
	"IPProtocolIPv6Routing":     layers.IPProtocolIPv6Routing,
	"IPProtocolIPv6Fragment":    layers.IPProtocolIPv6Fragment,
	"IPProtocolGRE":             layers.IPProtocolGRE,
	"IPProtocolESP":             layers.IPProtocolESP,
	"IPProtocolAH":              layers.IPProtocolAH,
	"IPProtocolICMPv6":          layers.IPProtocolICMPv6,
	"IPProtocolNoNextHeader":    layers.IPProtocolNoNextHeader,
	"IPProtocolIPv6Destination": layers.IPProtocolIPv6Destination,
	"IPProtocolOSPF":            layers.IPProtocolOSPF,
	"IPProtocolIPIP":            layers.IPProtocolIPIP,
	"IPProtocolEtherIP":         layers.IPProtocolEtherIP,
	"IPProtocolVRRP":            layers.IPProtocolVRRP,
	"IPProtocolSCTP":            layers.IPProtocolSCTP,
	"IPProtocolUDPLite":         layers.IPProtocolUDPLite,
	"IPProtocolMPLSInIP":        layers.IPProtocolMPLSInIP}

//Source match if packet's source IP is the same as the IP packet
//The expression that this predicate expects is a string with format XXX.YYY.ZZZ.SSS
func Source(expr string) (func(gopacket.Packet) bool, error) {

	srcIp := net.ParseIP(expr)
	if srcIp == nil {
		return nil, errors.Errorf("Could not parse given source IP: %v", srcIp)
	}

	return func(packet gopacket.Packet) bool {

		if ipv4Layer := packet.Layer(layers.LayerTypeIPv4); ipv4Layer == nil {
			return false
		} else {
			return packet.Layer(layers.LayerTypeIPv4).(*layers.IPv4).SrcIP.Equal(srcIp)
		}
	}, nil

}

//SourceNot is Source not
func SourceNot(expr string) (func(gopacket.Packet) bool, error) {
	f, err := Source(expr)

	if err != nil {
		return nil, err
	}

	return func(packet gopacket.Packet) bool {
		return !f(packet)
	}, nil
}

//Destination match if packet's destination IP is the same as the packet Destination IP packet
//The expression that this predicate expects is a string with format XXX.YYY.ZZZ.SSS
func Destination(expr string) (func(gopacket.Packet) bool, error) {

	dstIp := net.ParseIP(expr)
	if dstIp == nil {
		return nil, errors.Errorf("Could not parse given destination IP: %v", dstIp)
	}

	return func(packet gopacket.Packet) bool {
		if ipv4Layer := packet.Layer(layers.LayerTypeIPv4); ipv4Layer == nil {
			return false
		} else {
			return packet.Layer(layers.LayerTypeIPv4).(*layers.IPv4).DstIP.Equal(dstIp)
		}
	}, nil
}

//DestinationNot is Destination not
func DestinationNot(expr string) (func(gopacket.Packet) bool, error) {
	f, err := Destination(expr)

	if err != nil {
		return nil, err
	}

	return func(packet gopacket.Packet) bool {
		return !f(packet)
	}, nil

}

//stringToArray converts long string in a array split by comas
//The expression that this predicate expects is a string
func stringToArray(expr string) []string {
	var newArray []string

	for len(expr) > 0 {
		for i := range expr {
			if expr[i] == ',' {
				newArray = append(newArray, expr[:i])
				expr = expr[i+1:]
				break
			}
			if i == len(expr)-1 {
				newArray = append(newArray, expr[:i+1])
				expr = expr[i+1:]
				break
			}
		}

	}

	return newArray
}

//SourceNetwork match if packet come for the intended network specified in parameter
//The expression that this predicate expects is a string with format XXX.YYY.ZZZ.SSS/IJ
func SourceNetwork(expr string) (func(gopacket.Packet) bool, error) {

	_, srcNet, err := net.ParseCIDR(expr)

	if err != nil {
		return nil, err
	}

	return func(packet gopacket.Packet) bool {
		if ipv4Layer := packet.Layer(layers.LayerTypeIPv4); ipv4Layer == nil {
			return false
		} else {
			return srcNet.Contains(packet.Layer(layers.LayerTypeIPv4).(*layers.IPv4).SrcIP)
		}
	}, nil
}

//SourceNetworkNot is SourceNetwork not
func SourceNetworkNot(expr string) (func(gopacket.Packet) bool, error) {
	f, err := SourceNetwork(expr)

	if err != nil {
		return nil, err
	}

	return func(packet gopacket.Packet) bool {
		return !f(packet)
	}, nil
}

//DestinationNetwork match if the recipient is in the network specified in parameter
//The expression that this predicate expects is a string with format XXX.YYY.ZZZ.SSS/IJ
func DestinationNetwork(expr string) (func(gopacket.Packet) bool, error) {

	_, dstNet, err := net.ParseCIDR(expr)

	if err != nil {
		return nil, err
	}

	return func(packet gopacket.Packet) bool {
		if ipv4Layer := packet.Layer(layers.LayerTypeIPv4); ipv4Layer == nil {
			return false
		} else {
			return dstNet.Contains(packet.Layer(layers.LayerTypeIPv4).(*layers.IPv4).DstIP)
		}
	}, nil
}

//DestinationNetworkNot is Destination not
func DestinationNetworkNot(expr string) (func(gopacket.Packet) bool, error) {
	f, err := DestinationNetwork(expr)

	if err != nil {
		return nil, err
	}

	return func(packet gopacket.Packet) bool {
		return !f(packet)
	}, nil
}

//Protocol returns True is Packet's protocol is one of those specified
//expr egg : "IPProtocolTCP,IPProtocolEtherIP,IPProtocolNoNextHeader"
func Protocol(expr string) (func(gopacket.Packet) bool, error) {

	if strings.Count(expr, ",") == 0 {
		return nil, errors.New(" no ',' found")
	}
	protocolToCheck := stringToArray(expr)

	var protocols []layers.IPProtocol
	for i := 0; i < len(protocolToCheck); i++ {
		protocols = append(protocols,
			listofProtocol[protocolToCheck[i]])
	}

	return func(packet gopacket.Packet) bool {

		if ipv4Layer := packet.Layer(layers.LayerTypeIPv4); ipv4Layer == nil {
			return false
		} else {
			result := false
			for _, b := range protocols {
				if packet.Layer(layers.LayerTypeIPv4).(*layers.IPv4).Protocol == b {
					result = true
				}
			}
			return result
		}
	}, nil
}

//Ttlmin returns True if Packet's Time to live is higher than the specified one
//expr is the number of jumps or seconds
func TtlMin(expr string) (func(gopacket.Packet) bool, error) {
	nExpr, err := strconv.ParseUint(expr, 10, 8)

	if err != nil {
		return nil, err
	}

	parsedExpr := uint8(nExpr)

	return func(packet gopacket.Packet) bool {
		if ipv4Layer := packet.Layer(layers.LayerTypeIPv4); ipv4Layer == nil {
			return false
		} else {
			return packet.Layer(layers.LayerTypeIPv4).(*layers.IPv4).TTL >= parsedExpr
		}
	}, nil
}

//Ttlmax returns True if Packet's Time to live is lower than the specified one
//expr is the number of jumps or seconds
func TtlMax(expr string) (func(gopacket.Packet) bool, error) {
	nExpr, err := strconv.ParseUint(expr, 10, 8)

	if err != nil {
		return nil, err
	}

	parsedExpr := uint8(nExpr)

	return func(packet gopacket.Packet) bool {
		if ipv4Layer := packet.Layer(layers.LayerTypeIPv4); ipv4Layer == nil {
			return false
		} else {
			return packet.Layer(layers.LayerTypeIPv4).(*layers.IPv4).TTL <= parsedExpr
		}
	}, nil
}

//LengthMin return True if Packet's Length is higher than the specified one
// expr is just a number
func LengthMin(expr string) (func(gopacket.Packet) bool, error) {
	nExpr, err := strconv.ParseUint(expr, 10, 16)

	if err != nil {
		return nil, err
	}

	parsedExpr := uint16(nExpr)

	return func(packet gopacket.Packet) bool {
		if ipv4Layer := packet.Layer(layers.LayerTypeIPv4); ipv4Layer == nil {
			return false
		} else {
			return packet.Layer(layers.LayerTypeIPv4).(*layers.IPv4).Length >= parsedExpr
		}
	}, nil
}

//LengthMax return True if Packet's Length is lower than the specified one
//expr is just a number
func LengthMax(expr string) (func(gopacket.Packet) bool, error) {
	nExpr, err := strconv.ParseUint(expr, 10, 16)

	if err != nil {
		return nil, err
	}

	parsedExpr := uint16(nExpr)

	return func(packet gopacket.Packet) bool {
		if ipv4Layer := packet.Layer(layers.LayerTypeIPv4); ipv4Layer == nil {
			return false
		} else {
			return packet.Layer(layers.LayerTypeIPv4).(*layers.IPv4).Length <= parsedExpr
		}
	}, nil
}

//Flag returns True is Packet's flag is one of those specified
//expr egg : "IPv4EvilBit,IPv4DontFragment"
func Flag(expr string) (func(gopacket.Packet) bool, error) {

	if strings.Count(expr, ",") == 0 {
		return nil, errors.New(" no ',' found")
	}

	flagToCheck := stringToArray(expr)

	var flags []layers.IPv4Flag
	for i := 0; i < len(flagToCheck); i++ {
		flags = append(flags, listofFlag[flagToCheck[i]])
	}

	return func(packet gopacket.Packet) bool {
		if ipv4Layer := packet.Layer(layers.LayerTypeIPv4); ipv4Layer == nil {
			return false
		} else {
			result := false
			for _, b := range flags {
				if packet.Layer(layers.LayerTypeIPv4).(*layers.IPv4).Flags == b {
					result = true
				}
			}
			return result
		}
	}, nil

}

//CheckTOS returns True if Packet's TOS matches
//expr format 8bits egg:"10110011"
func Tos(expr string) (func(gopacket.Packet) bool, error) {

	nExpr, err := strconv.ParseUint(expr, 10, 8)
	parsedExpr := uint8(nExpr)

	if err != nil {
		return nil, err
	}

	return func(packet gopacket.Packet) bool {
		if ipv4Layer := packet.Layer(layers.LayerTypeIPv4); ipv4Layer == nil {
			return false
		} else {
			return packet.Layer(layers.LayerTypeIPv4).(*layers.IPv4).TOS == parsedExpr
		}
	}, nil

}
