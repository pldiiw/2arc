package tcp

import (
	"errors"
	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
	"reflect"
	"strconv"
	"strings"
)

var Predicates = map[string]func(expr string) (func(gopacket.Packet) bool, error){
	"Source":           Source,
	"SourceRange":      SourceRange,
	"SourceNot":        SourceNot,
	"Destination":      Destination,
	"DestinationRange": DestinationRange,
	"DestinationNot":   DestinationNot,
	"Control":          Control,
	"ControlNot":       ControlNot,
	"ControlOr":        ControlOr,
	"ControlXOr":       ControlXOr,
}

var controlBits = []string{"FIN", "SYN", "RST", "PSH", "ACK", "URG", "ECE", "CWR", "NS"}

//Source matches when a TCP packet has its source port field corresponding to the port given as an expression.
//The expression that this predicate expects is a number between 0 and 65535 corresponding to a TCP port.
func Source(expr string) (func(gopacket.Packet) bool, error) {

	parsedExpr, err := strconv.ParseInt(expr, 10, 16)
	srcPort := layers.TCPPort(parsedExpr)
	if err != nil {
		return nil, err
	}
	return (func(packet gopacket.Packet) bool {
		if tcpLayer := packet.Layer(layers.LayerTypeTCP); tcpLayer == nil {
			return false
		} else {
			return tcpLayer.(*layers.TCP).SrcPort == srcPort
		}
	}), nil
}

//SourceRange matches when a TCP packet has its source port field is included in a range of ports given as an expression.
//The expression that this predicate expects is two numbers between 0 and 65535 corresponding to TCP ports, the numbers must be
//different, separated by a dash, and the first one must be lesser than the second one.
//Exemple : 10-500
func SourceRange(expr string) (func(gopacket.Packet) bool, error) {
	split := strings.Index(expr, "-")
	if split == -1 {
		return nil, errors.New("no '-' find")
	}

	first, err := strconv.ParseInt(expr[:split], 10, 16)
	if err != nil {
		return nil, err
	}
	last, err := strconv.ParseInt(expr[split+1:], 10, 16)
	if err != nil {
		return nil, err
	}
	srcFirstPort := layers.TCPPort(first)
	srcLastPort := layers.TCPPort(last)

	return (func(packet gopacket.Packet) bool {
		if tcpLayer := packet.Layer(layers.LayerTypeTCP); tcpLayer == nil {
			return false
		} else {
			return tcpLayer.(*layers.TCP).SrcPort >= srcFirstPort && tcpLayer.(*layers.TCP).SrcPort < srcLastPort
		}
	}), nil
}

// SourceNot is Source's complement.
func SourceNot(expr string) (func(gopacket.Packet) bool, error) {
	f, err := Source(expr)
	if err != nil {
		return nil, err
	}

	return (func(packet gopacket.Packet) bool {
		return !f(packet)
	}), nil
}

//Destination matches when a TCP packet has its destination port field corresponding to the port given as an expression.
//The expression that this predicate expects is a number between 0 and 65535 corresponding to a TCP port.
func Destination(expr string) (func(gopacket.Packet) bool, error) {

	parsedExpr, err := strconv.ParseInt(expr, 10, 16)
	if err != nil {
		return nil, err
	}
	dstPort := layers.TCPPort(parsedExpr)
	return (func(packet gopacket.Packet) bool {
		if tcpLayer := packet.Layer(layers.LayerTypeTCP); tcpLayer == nil {
			return false
		} else {
			return tcpLayer.(*layers.TCP).DstPort == dstPort
		}
	}), nil
}

//DestinationRange matches when a TCP packet has its destination port field is included in a range of ports given as an expression.
//The expression that this predicate expects is two numbers between 0 and 65535 corresponding to TCP ports, the numbers must be
//different, separated by a dash, and the first one must be lesser than the second one.
//Exemple : 10-500
func DestinationRange(expr string) (func(gopacket.Packet) bool, error) {
	split := strings.Index(expr, "-")
	if split == -1 {
		return nil, errors.New("no '-' find")
	}

	first, err := strconv.ParseInt(expr[:split], 10, 16)
	if err != nil {
		return nil, err
	}
	last, err := strconv.ParseInt(expr[split+1:], 10, 16)
	if err != nil {
		return nil, err
	}

	dstFirstPort := layers.TCPPort(first)
	dstLastPort := layers.TCPPort(last)

	return (func(packet gopacket.Packet) bool {
		if tcpLayer := packet.Layer(layers.LayerTypeTCP); tcpLayer == nil {
			return false
		} else {
			return tcpLayer.(*layers.TCP).DstPort >= dstFirstPort && tcpLayer.(*layers.TCP).DstPort < dstLastPort
		}
	}), nil
}

// DestinationNot is Destination's complement.
func DestinationNot(expr string) (func(gopacket.Packet) bool, error) {
	f, err := Destination(expr)
	if err != nil {
		return nil, err
	}

	return (func(packet gopacket.Packet) bool {
		return !f(packet)
	}), nil
}

// Control matches when a TCP packet has all of its control bits fields given in an expression in state true.
// The expression that this predicate expects is a comma-separated list of one or more control bits.
// Example: FIN,PSH,CWR
// Here's a list of all the control bits:
// - FIN
// - SYN
// - RST
// - PSH
// - ACK
// - URG
// - ECE
// - CWR
// - NS

func Control(expr string) (func(gopacket.Packet) bool, error) {
	var states []string
	for len(expr) > 0 {
		for i := range expr {
			if expr[i] == ',' {
				states = append(states, expr[:i])
				expr = expr[i+1:]
				break
			}
			if i == len(expr)-1 {
				states = append(states, expr[:i+1])
				expr = expr[i+1:]
				break
			}
		}
	}
	for _, state := range states {
		match := false
		for _, controlBit := range controlBits {
			if state == controlBit {
				match = true
				break
			}
		}
		if !match {
			return nil, errors.New("Given control bit does not exist: " + state)
		}
	}
	return (func(packet gopacket.Packet) bool {
		if tcpLayer := packet.Layer(layers.LayerTypeTCP); tcpLayer == nil {
			return false
		} else {
			for i := range states {
				if !reflect.ValueOf(tcpLayer.(*layers.TCP)).Elem().FieldByName(states[i]).Bool() {
					return false
				}
			}
			return true
		}
	}), nil
}

// ControlNot is Control's complement.
func ControlNot(expr string) (func(gopacket.Packet) bool, error) {
	f, err := Control(expr)
	if err != nil {
		return nil, err
	}

	return (func(packet gopacket.Packet) bool {
		return !f(packet)
	}), nil
}

// ControlOr matches when a TCP packet has one of its control bits fields given in an expression in state true.
// The expression that this predicate expects is a comma-separated list of one or more control bits.
// Example: FIN,PSH,CWR
// Here's a list of all the control bits:
// - FIN
// - SYN
// - RST
// - PSH
// - ACK
// - URG
// - ECE
// - CWR
// - NS
func ControlOr(expr string) (func(gopacket.Packet) bool, error) {
	var states []string
	for len(expr) > 0 {
		for i := range expr {
			if expr[i] == ',' {
				states = append(states, expr[:i])
				expr = expr[i+1:]
				break
			}
			if i == len(expr)-1 {
				states = append(states, expr[:i+1])
				expr = expr[i+1:]
				break
			}
		}
	}
	for _, state := range states {
		match := false
		for _, controlBit := range controlBits {
			if state == controlBit {
				match = true
				break
			}
		}
		if !match {
			return nil, errors.New("Given control bit does not exist: " + state)
		}
	}
	return (func(packet gopacket.Packet) bool {
		if tcpLayer := packet.Layer(layers.LayerTypeTCP); tcpLayer == nil {
			return false
		} else {
			for i := range states {
				if reflect.ValueOf(packet.Layer(layers.LayerTypeTCP).(*layers.TCP)).Elem().FieldByName(states[i]).Bool() {
					return true
				}
			}
			return false
		}
	}), nil
}

// ControlXOr matches when a TCP packet has only one of its control bits fields given in an expression in state true.
// The expression that this predicate expects is a comma-separated list of one or more control bits.
// Example: FIN,PSH,CWR
// Here's a list of all the control bits:
// - FIN
// - SYN
// - RST
// - PSH
// - ACK
// - URG
// - ECE
// - CWR
// - NS
func ControlXOr(expr string) (func(gopacket.Packet) bool, error) {
	var states []string
	for len(expr) > 0 {
		for i := range expr {
			if expr[i] == ',' {
				states = append(states, expr[:i])
				expr = expr[i+1:]
				break
			}
			if i == len(expr)-1 {
				states = append(states, expr[:i+1])
				expr = expr[i+1:]
				break
			}
		}
	}
	for _, state := range states {
		match := false
		for _, controlBit := range controlBits {
			if state == controlBit {
				match = true
				break
			}
		}
		if !match {
			return nil, errors.New("Given control bit does not exist: " + state)
		}
	}
	return (func(packet gopacket.Packet) bool {
		if tcpLayer := packet.Layer(layers.LayerTypeTCP); tcpLayer == nil {
			return false
		} else {
			count := 0
			for i := range states {
				if reflect.ValueOf(packet.Layer(layers.LayerTypeTCP).(*layers.TCP)).Elem().FieldByName(states[i]).Bool() {
					count++
				}
			}
			if count != 1 {
				return false
			} else {
				return true
			}
		}
	}), nil
}
