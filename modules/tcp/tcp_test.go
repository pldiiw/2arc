package tcp

import (
	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
	"os"
	"testing"
)

var testPacket gopacket.Packet
var testUDPPacket gopacket.Packet
var testNoTransportPacket gopacket.Packet

func TestMain(m *testing.M) {
	buf := gopacket.NewSerializeBuffer()
	opts := gopacket.SerializeOptions{}

	gopacket.SerializeLayers(buf, opts, &layers.TCP{})
	testPacket = gopacket.NewPacket(buf.Bytes(), layers.LayerTypeTCP, gopacket.Default)
	testPacket.Layer(layers.LayerTypeTCP).(*layers.TCP).SrcPort = layers.TCPPort(500)
	testPacket.Layer(layers.LayerTypeTCP).(*layers.TCP).DstPort = layers.TCPPort(501)
	testPacket.Layer(layers.LayerTypeTCP).(*layers.TCP).FIN = true
	testPacket.Layer(layers.LayerTypeTCP).(*layers.TCP).SYN = true
	testPacket.Layer(layers.LayerTypeTCP).(*layers.TCP).RST = true

	gopacket.SerializeLayers(buf, opts, &layers.UDP{})
	testUDPPacket = gopacket.NewPacket(buf.Bytes(), layers.LayerTypeUDP, gopacket.Default)

	gopacket.SerializeLayers(buf, opts, &layers.Ethernet{}, &layers.IPv4{})
	testNoTransportPacket = gopacket.NewPacket(buf.Bytes(), layers.LayerTypeIPv4, gopacket.Default)

	os.Exit(m.Run())
}

func TestSourceTrue(t *testing.T) {
	t.Log(`Passing "500" as an expression to Source should match an TCP segment having its source port field set to 500`)
	f, err := Source("500")
	if err != nil {
		t.Errorf(`Source could not parse "500": %v`, err)
	}

	if !f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestSourceFalse(t *testing.T) {
	t.Log(`Passing "1" as an expression to Source shouldn't match an TCP segment having its source port field not set to 1`)
	f, err := Source("1")
	if err != nil {
		t.Errorf(`Source could not parse "1": %v`, err)
	}

	if f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestSourceErr(t *testing.T) {
	t.Log(`Passing anything else than a source port id to Source should return an error.`)

	_, err := Source("fail")

	if err == nil {
		t.Error(`Source parsed "fail" as a valid expression, it shouldn't: only a number is valid.`)
	}
}

func TestSourceRangeTrue(t *testing.T) {
	t.Log(`Passing "500-510" as an expression to SourceRange should match an TCP segment having its source port field included
between 500 and 510`)
	f, err := SourceRange("500-510")
	if err != nil {
		t.Errorf(`SourceRange could not parse "500-510": %v`, err)
	}

	if !f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestSourceRangeFalse(t *testing.T) {
	t.Log(`Passing "1-2" as an expression to SourceRange shouldn't match an TCP segment having its source port field not include
between 1 and 2`)
	f, err := SourceRange("1-2")
	if err != nil {
		t.Errorf(`SourceRange could not parse "1-2": %v`, err)
	}

	if f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestSourceRangeErr(t *testing.T) {
	t.Log(`Passing anything else than two source port ids to SourceRange should return an error.`)

	_, err := SourceRange("fail")

	if err == nil {
		t.Error(`SourceRange parsed "fail" as a valid expression, it shouldn't: only numbers are valid.`)
	}
}

func TestSourceNotTrue(t *testing.T) {
	t.Log(`Passing "1" as an expression to SourceNot should match an TCP segment having its source port field not set to 1`)
	f, err := SourceNot("1")
	if err != nil {
		t.Errorf(`SourceNot could not parse "1": %v`, err)
	}

	if !f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestSourceNotFalse(t *testing.T) {
	t.Log(`Passing "500" as an expression to SourceNot shouldn't match an TCP segment having its source port field set to 500`)
	f, err := SourceNot("500")
	if err != nil {
		t.Errorf(`SourceNot could not parse "500": %v`, err)
	}

	if f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestSourceNotErr(t *testing.T) {
	t.Log(`Passing anything else than a source port id to SourceNot should return an error.`)

	_, err := SourceNot("fail")

	if err == nil {
		t.Error(`SourceNot parsed "fail" as a valid expression, it shouldn't: only a number is valid.`)
	}
}

func TestDestinationTrue(t *testing.T) {
	t.Log(`Passing "501" as an expression to Destination should match an TCP segment having its destination port field set to 501`)
	f, err := Destination("501")
	if err != nil {
		t.Errorf(`Destination could not parse "501": %v`, err)
	}

	if !f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestDestinationFalse(t *testing.T) {
	t.Log(`Passing "1" as an expression to Destination shouldn't match an TCP segment having its destination port field not set 
to 1`)
	f, err := Destination("1")
	if err != nil {
		t.Errorf(`Destination could not parse "1": %v`, err)
	}

	if f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestDestinationErr(t *testing.T) {
	t.Log(`Passing anything else than a destination port id to Destination should return an error.`)

	_, err := Destination("fail")

	if err == nil {
		t.Error(`Destination parsed "fail" as a valid expression, it shouldn't: only a number is valid.`)
	}
}

func TestDestinationRangeTrue(t *testing.T) {
	t.Log(`Passing "500-510" as an expression to DestinationRange should match an TCP segment having its destination port field 
include between 500 and 510`)
	f, err := DestinationRange("500-510")
	if err != nil {
		t.Errorf(`DestinationRange could not parse "500-510": %v`, err)
	}

	if !f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestDestinationRangeFalse(t *testing.T) {
	t.Log(`Passing "1-2" as an expression to DestinationRange shouldn't match an TCP segment having its destination port field not 
include between 1 and 2`)
	f, err := DestinationRange("1-2")
	if err != nil {
		t.Errorf(`DestinationRange could not parse "1-2": %v`, err)
	}

	if f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestDestinationRangeErr(t *testing.T) {
	t.Log(`Passing anything else than two destination port id separate by a dash to DestinationRange should return an error.`)

	_, err := DestinationRange("fail")

	if err == nil {
		t.Error(`DestinationRange parsed "fail" as a valid expression, it shouldn't: only numbers are valid.`)
	}
}

func TestDestinationNotTrue(t *testing.T) {
	t.Log(`Passing "1" as an expression to DestinationNot should match an TCP segment having its destination port field not set to 1`)
	f, err := DestinationNot("1")
	if err != nil {
		t.Errorf(`DestinationNot could not parse "1": %v`, err)
	}

	if !f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestDestinationNotFalse(t *testing.T) {
	t.Log(`Passing "501" as an expression to DestinationNot shouldn't match an TCP segment having its destination port field set 
to 501`)
	f, err := DestinationNot("501")
	if err != nil {
		t.Errorf(`DestinationNot could not parse "501": %v`, err)
	}

	if f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestDestinationNotErr(t *testing.T) {
	t.Log(`Passing anything else than a destination port id to DestinationNot should return an error.`)

	_, err := DestinationNot("fail")

	if err == nil {
		t.Error(`DestinationNot parsed "fail" as a valid expression, it shouldn't: only a number is valid.`)
	}
}

func TestControlTrue(t *testing.T) {
	t.Log(`Passing "FIN,SYN" as an expression to Control should match an TCP segment having its conrole bits fields FIN and 
SYN set to true`)
	f, err := Control("FIN,SYN")
	if err != nil {
		t.Errorf(`Control could not parse "FIN,SYN": %v`, err)
	}

	if !f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestControlFalse(t *testing.T) {
	t.Log(`Passing "PSH,RST" as an expression to Control shouldn't match an TCP segment having its controle bits fields PSH and 
 RST not set to true`)
	f, err := Control("PSH,RST")
	if err != nil {
		t.Errorf(`Control could not parse "PSH,RST": %v`, err)
	}

	if f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestControlErr(t *testing.T) {
	t.Log(`Passing anything else than a list of control bits to Control should return an error.`)

	_, err := Control("fail")

	if err == nil {
		t.Error(`Control parsed "fail" as a valid expression, it shouldn't: only a list of controlbits names is valid.`)
	}
}

func TestControlNotTrue(t *testing.T) {
	t.Log(`Passing "PSH,RST" as an expression to ControlNot should match an TCP segment having its conrole bits fields PSH and 
RST not set to true`)
	f, err := ControlNot("PSH,RST")
	if err != nil {
		t.Errorf(`Control could not parse "PSH,RST": %v`, err)
	}

	if !f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestControlNotFalse(t *testing.T) {
	t.Log(`Passing "FIN,SYN" as an expression to ControlNot shouldn't match an TCP segment having its controle bits fields FIN and 
 SYN set to true`)
	f, err := ControlNot("FIN,SYN")
	if err != nil {
		t.Errorf(`ControlNot could not parse "FIN,SYN": %v`, err)
	}

	if f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestControlNotErr(t *testing.T) {
	t.Log(`Passing anything else than a list of control bits to ControlNot should return an error.`)

	_, err := ControlNot("fail")

	if err == nil {
		t.Error(`ControlNot parsed "fail" as a valid expression, it shouldn't: only a list of controlbits names is valid.`)
	}
}

func TestControlOrTrue(t *testing.T) {
	t.Log(`Passing "FIN,PSH" as an expression to ControlOr should match an TCP segment having its conrole bits fields FIN or PSH 
or both set to true`)
	f, err := ControlOr("FIN,PSH")
	if err != nil {
		t.Errorf(`ControlOr could not parse "FIN,PSH": %v`, err)
	}

	if !f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestControlOrFalse(t *testing.T) {
	t.Log(`Passing "PSH,ACK" as an expression to ControlOr shouldn't match an TCP segment having its controle bits fields PSH or 
 ACK or both not set to true`)
	f, err := ControlOr("PSH,ACK")
	if err != nil {
		t.Errorf(`ControlOr could not parse "PSH,ACK": %v`, err)
	}

	if f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestControlOrErr(t *testing.T) {
	t.Log(`Passing anything else than a list of control bits to ControlOr should return an error.`)

	_, err := ControlOr("fail")

	if err == nil {
		t.Error(`ControlOr parsed "fail" as a valid expression, it shouldn't: only a list of controlbits names is valid.`)
	}
}

func TestControlXOrTrue(t *testing.T) {
	t.Log(`Passing "FIN,PSH" as an expression to ControlXOr should match an TCP segment having its conrole bits fields FIN or PSH 
set to true`)
	f, err := ControlXOr("FIN,PSH")
	if err != nil {
		t.Errorf(`ControlXOr could not parse "FIN,PSH": %v`, err)
	}

	if !f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestControlXOrFalse(t *testing.T) {
	t.Log(`Passing "FIN,SYN" as an expression to ControlxOr shouldn't match an TCP segment having its controle bits fields FIN or 
 SYN not set to true`)
	f, err := ControlXOr("FIN,SYN")
	if err != nil {
		t.Errorf(`ControlXOr could not parse "FIN,SYN": %v`, err)
	}

	if f(testPacket) {
		t.Error(`The given packet didn't match.`)
	}

}

func TestControlXOrErr(t *testing.T) {
	t.Log(`Passing anything else than a list of control bits to ControlXOr should return an error.`)

	_, err := ControlXOr("fail")

	if err == nil {
		t.Error(`ControlXOr parsed "fail" as a valid expression, it shouldn't: only a list of controlbits names is valid.`)
	}
}
func TestLackingTCPLayer(t *testing.T) {
	t.Log(`Passing a packet not having an TCP datagram should return false for all predicates.`)

	for expr, fn := range map[string]func(string) (func(packet gopacket.Packet) bool, error){
		"500":     Source,
		"480-510": SourceRange,
		"501":     Destination,
		"481-510": DestinationRange,
		"FIN,SYN": Control,
		"FIN,PSH": ControlOr,
		"FIN,URG": ControlXOr,
	} {
		f, err := fn(expr)
		if err != nil {
			t.Error(err)
		}
		if f(testUDPPacket) {
			t.Error(`Packet shouldn't have matched: the transport isn't TCP`)
		}
		if f(testNoTransportPacket) {
			t.Error(`Packet shouldn't have matched: there's no transport layer`)
		}
	}

}
