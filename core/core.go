package core

import (
	"2arc/config"
	"2arc/pipeline"
	"fmt"
	"github.com/google/gopacket"
	"github.com/google/gopacket/pcap"
	"github.com/subgraph/go-nfnetlink/nfqueue"
	"io/ioutil"
	"os"
	"os/exec"
	"os/signal"
	"reflect"
	"strconv"
)

var builtinChainsNames = []string{"in", "out", "forward"}
var builtinNFTChainsMapping = map[string]string{"in": "input", "out": "output", "forward": "forward"}

// setup takes care of reading parsing the configuration file and returning the
// resulting processing pipelines
func setup(configFile string) map[string]map[string]func(gopacket.Packet) bool {
	var err error

	// Read and parse config
	configContent, err := ioutil.ReadFile(configFile)
	if err != nil {
		fmt.Println("Error reading raw config file")
		panic(err)
	}

	configuration, err := config.Parse(string(configContent))
	if err != nil {
		fmt.Println("Error parsing config")
		panic(err)
	}

	// Initialize the resulting configuration
	zones, err := pipeline.Init(configuration)
	if err != nil {
		fmt.Println("Error initializing the pipelines with the given configuration")
		panic(err)
	}

	return zones
}

// Live is the entrypoint to start the firewall. It asks for the config, and
// then starts giving verdict to packets
func Live(configFile string) {
	var err error

	zones := setup(configFile)

	// Little stub for zone support, we only support one for the moment: the
	// "default" zone
	chains := zones["default"]

	err = exec.Command("nft", "add", "table", "inet", "filter").Run()
	if err != nil {
		fmt.Println("Could not create filter table in nftables")
		panic(err)
	}
	defer exec.Command("nft", "delete", "table", "inet", "filter").Run()

	dispatching := make([]reflect.SelectCase, 0)
	dispatchingFns := make([]func(gopacket.Packet) bool, 0)

	for i, name := range builtinChainsNames {
		chainfn := chains[name]
		if chainfn == nil {
			continue
		}

		q := nfqueue.NewNFQueue(uint16(i))
		c, err := q.Open()
		if err != nil {
			fmt.Println("Could not open nfqueue")
			panic(err)
		}
		defer q.Close()

		nameInNFT := builtinNFTChainsMapping[name]

		err = exec.Command("nft", "add", "chain", "inet", "filter", nameInNFT, "{", "type", "filter", "hook", nameInNFT, "priority", "0", ";", "policy", "accept", ";", "}").Run()
		if err != nil {
			fmt.Printf("Error creating a new chain named %s inside table filter in nftables\n", nameInNFT)
			panic(err)
		}
		defer exec.Command("nft", "delete", "chain", "inet", "filter", nameInNFT).Run()

		err = exec.Command("nft", "add", "rule", "inet", "filter", nameInNFT, "counter", "queue", "num", strconv.Itoa(i)).Run()
		if err != nil {
			fmt.Printf("Error adding nfqueue rule to chain %s in nftables\n", nameInNFT)
			panic(err)
		}
		defer exec.Command("nft", "delete", "inet", "filter", nameInNFT, "counter", "queue", "num", strconv.Itoa(i)).Run()

		dispatching = append(dispatching, reflect.SelectCase{Dir: reflect.SelectRecv, Chan: reflect.ValueOf(c)})
		dispatchingFns = append(dispatchingFns, chainfn)
	}

	// Trap signals for clean exit
	trap := make(chan os.Signal, 1)
	signal.Notify(trap, os.Interrupt, os.Kill)

	dispatching = append(dispatching, reflect.SelectCase{Dir: reflect.SelectRecv, Chan: reflect.ValueOf(trap)})

	// Start processing packets
	for {
		dispatchIndex, v, _ := reflect.Select(dispatching)

		if dispatchIndex >= len(dispatchingFns) { // trap case
			fmt.Println("Shutting down...")
			return
		}

		dispatchFn := dispatchingFns[dispatchIndex]

		if packet := v.Interface().(*nfqueue.NFQPacket); dispatchFn(packet.Packet) {
			packet.Accept()
		} else {
			packet.Drop()
		}
	}
}

// OffWire takes care of reading a given pcap, and, after configuring the
// firewall, tell which captured packets would be dropped or accepted.
func OffWire(configFile string, pcapFile string) {
	var err error

	zones := setup(configFile)

	pcapHandle, err := pcap.OpenOffline(pcapFile)
	if err != nil {
		fmt.Printf(`Cound not open given pcap file "%s"`, pcapFile)
		panic(err)
	}

	pcapPacketSource := gopacket.NewPacketSource(pcapHandle, pcapHandle.LinkType())

	stats := make(map[string]map[string]map[string]int)

	for packet := range pcapPacketSource.Packets() {
		fmt.Println(packet)

		for zoneName, chains := range zones {
			fmt.Printf("\tVerdicts for zone %s: \n", zoneName)

			if stats[zoneName] == nil {
				stats[zoneName] = make(map[string]map[string]int)
			}

			for chainName, chainFn := range chains {
				var verdict string

				if chainFn(packet) {
					verdict = config.Accept
				} else {
					verdict = config.Drop
				}

				if stats[zoneName][chainName] == nil {
					stats[zoneName][chainName] = make(map[string]int)
				}
				stats[zoneName][chainName][verdict]++

				fmt.Printf("\t\tChain %s: %s\n", chainName, verdict)
			}
		}
		fmt.Println("--")
	}

	fmt.Println("Final stats:")
	for zoneName, chains := range stats {
		fmt.Printf("\tStats for zone %s\n", zoneName)

		var totalAccept, totalDrop int
		for chainName, verdictsStats := range chains {
			fmt.Printf("\t\tChain %s: %v %s, %v %s\n", chainName, verdictsStats[config.Accept], config.Accept, verdictsStats[config.Drop], config.Drop)
			totalAccept += verdictsStats[config.Accept]
			totalDrop += verdictsStats[config.Drop]
		}
		fmt.Printf("\t\tTotal: %v %s, %v %s\n", totalAccept, config.Accept, totalDrop, config.Drop)
	}
}
