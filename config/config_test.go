package config

import (
	"os"
	"reflect"
	"testing"
)

var testInput1 string
var testInput2 string
var testInput3 string
var testInput4 string
var testInput5 string
var testInput6 string
var testInput7 string
var testInput8 string
var testInput9 string
var testInput10 string
var testInput11 string
var testInput12 string
var testInput13 string
var testInput14 string
var testExpected1 Configuration
var testExpected2 Configuration
var testExpected4 Configuration
var testExpected5 Configuration

func TestMain(m *testing.M) {
	testInput1 = `
ZONE public
    IFACES wlan0
    CHAIN in
      DROP

ZONE test
    IFACES test
    CHAIN in
      DROP ipv4/SourceNetwork 192.168.10.0/24 tcp/state FINISHED
      DROP`

	testExpected1 = Configuration{
		Zones: []Zone{{
			"public",
			[]string{"wlan0"},
			[]Chain{{
				"in",
				[]Rule{{
					"DROP",
					[]Condition(nil),
				}},
			}},
		}, {
			"test",
			[]string{"test"},
			[]Chain{{
				"in",
				[]Rule{{
					"DROP",
					[]Condition{{
						"ipv4",
						"SourceNetwork",
						"192.168.10.0/24",
					}, {
						"tcp",
						"state",
						"FINISHED",
					}},
				}, {
					"DROP",
					[]Condition(nil),
				}}}}}}}

	testInput2 = `
ZONE public
    IFACES wlan0
    CHAIN in
      DROP
    CHAIN out
      DROP`

	testExpected2 = Configuration{
		Zones: []Zone{{
			"public",
			[]string{"wlan0"},
			[]Chain{{
				"in",
				[]Rule{{
					"DROP",
					[]Condition(nil),
				}}}, {
				"out",
				[]Rule{{
					"DROP",
					[]Condition(nil),
				}}}}}}}

	testInput3 = `
ZONE public
    IFACES wlan0
    CHAIN in
      in`

	testInput4 = `
#this is a full line comment
ZONE public
    IFACES wlan0
    CHAIN in
      DROP`

	testExpected4 = Configuration{
		Zones: []Zone{{
			"public",
			[]string{"wlan0"},
			[]Chain{{
				"in",
				[]Rule{{
					"DROP",
					[]Condition(nil),
				}}}}}}}

	testInput5 = `
ZONE public
    IFACES wlan0 #this is a partial line comment
    CHAIN in
      DROP`

	testExpected5 = Configuration{
		Zones: []Zone{{
			"public",
			[]string{"wlan0"},
			[]Chain{{
				"in",
				[]Rule{{
					"DROP",
					[]Condition(nil),
				}}}}}}}
	testInput6 = `
ZONE public
    IFACES wlan0
    CHAIN in`

	testInput7 = `
ZONE public
	IFACES wlan0
	CHAIN in
		DROP ipv4 192.168.10.0/24
		DROP`

	testInput8 = `
ZONE public
	IFACES wlan0
	CHAIN in
		DROP ipv4.SourceNetwork 192.168.10.0/24
		DROP`

	testInput9 = `
ZONE public
	IFACES
	CHAIN in
		DROP`

	testInput10 = `
ZONE public
	IFACES wlan0
	CHAIN in
		anOtherChain`

	testInput11 = `
ZONE public
	CHAIN in
		DROP`

	testInput12 = `
ZONE public
	IFACES wlan0`

	testInput13 = `
ZONE public
	IFACES wlan0
	CHAIN in
		DROP
	WrongOption
`
	testInput14 = `
ZONE public
	IFACES wlan0
	CHAIN in
		DROP ipv4.SourceNetwork
		DROP
`
	os.Exit(m.Run())
}

func TestMultipleZoneHandling(t *testing.T) {

	output, err := Parse(testInput1)
	if !reflect.DeepEqual(output, testExpected1) || err != nil {
		t.Error("Program failed to support multiple zones.")
	}
}

func TestMultipleChainHandling(t *testing.T) {

	output, err := Parse(testInput2)
	if !reflect.DeepEqual(output, testExpected2) || err != nil {
		t.Error("Program failed to support multiple chains.")
	}
}

func TestChainLoopCall(t *testing.T) {

	_, err := Parse(testInput3)
	if err == nil {
		t.Error("Program failed to prevent from loop calling chains.")
	}
}

func TestFullLineComment(t *testing.T) {

	output, err := Parse(testInput4)
	if !reflect.DeepEqual(output, testExpected4) || err != nil {
		t.Error("Program failed to support full line comment.")
	}
}

func TestPartialLineComment(t *testing.T) {

	output, err := Parse(testInput5)
	if !reflect.DeepEqual(output, testExpected5) || err != nil {
		t.Error("Program failed to support partail line comment.")
	}
}

func TestChainWithoutAction(t *testing.T) {

	_, err := Parse(testInput6)
	if err == nil {
		t.Error("Program failed to detect a chain without any actions.")
	}
}

func TestRuleWithoutPredicate(t *testing.T) {

	_, err := Parse(testInput7)
	if err == nil {
		t.Error("Program failed to detect a missing predicate.")
	}
}

func TestRuleWithWrongSeparator(t *testing.T) {

	_, err := Parse(testInput8)
	if err == nil {
		t.Error("Program failed to detect a wrong predicate separator.")
	}
}

func TestZonesWithEmptyInterfaces(t *testing.T) {

	_, err := Parse(testInput9)
	if err == nil {
		t.Error("Program failed to detect a zone without interfaces.")
	}
}

func TestNonImplementedChainCalling(t *testing.T) {

	_, err := Parse(testInput10)
	if err == nil {
		t.Error("Program failed to detect a non implemented chain calling.")
	}
}

func TestZonesWithoutInterfacesOption(t *testing.T) {

	_, err := Parse(testInput11)
	if err == nil {
		t.Error("Program failed to detect a zone without interfaces option.")
	}
}

func TestZonesWithoutChainOption(t *testing.T) {

	_, err := Parse(testInput12)
	if err == nil {
		t.Error("Program failed to detect a zone without chain option.")
	}
}

func TestZonesWithWrongOption(t *testing.T) {

	_, err := Parse(testInput13)
	if err == nil {
		t.Error("Program failed to detect a zone with wrong zone option.")
	}
}

func TestConditionWithoutExpression(t *testing.T) {

	_, err := Parse(testInput14)
	if err == nil {
		t.Error("Program failed to detect a condition without expression.")
	}
}
