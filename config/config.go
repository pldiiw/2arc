package config

import (
	"errors"
	"strings"
)

const (
	commentDelimiter = '#'
	Accept           = "ACCEPT"
	Drop             = "DROP"
)

type Configuration struct {
	Zones []Zone
}

type Zone struct {
	Name       string
	Interfaces []string
	Chains     []Chain
}

type Rule struct {
	Action     string
	Conditions []Condition
}

type Chain struct {
	Name  string
	Rules []Rule
}

type Condition struct {
	Module     string
	Predicate  string
	Expression string
}

// getConf Read the config file return by getConfFile and delete every blank
// line and comments then return it
func getConf(file string) []string {
	confFile := strings.Split(file, "\n")
	var cleanConfFile []string
	for i := 0; len(confFile) > i; i++ {
		if confFile[i] != "" {
			for char := 0; char < len(confFile[i]); char++ {
				if (confFile[i][char]) == commentDelimiter {
					break
				}
				if string(confFile[i][char]) != " " && string(confFile[i][char]) != "\t" {
					if index := strings.Index(confFile[i], string(commentDelimiter)); index > -1 {
						cleanConfFile = append(cleanConfFile, confFile[i][:index])
					} else {
						cleanConfFile = append(cleanConfFile, confFile[i])
					}
					break
				}
			}
		}
	}
	return cleanConfFile
}

// parseRules take a rule string in parameter and parse in the rule struct
func parseRules(ruleValue string) (Rule, error) {
	ruleValue = strings.TrimSpace(ruleValue)
	var myRule Rule
	ruleParse := strings.Split(ruleValue, " ")
	myRule.Action = ruleParse[0]
	for i := 1; len(ruleParse) > i; i += 2 {
		var cond Condition
		modAndPre := strings.Split(ruleParse[i], "/")

		if len(modAndPre) != 2 {
			return Rule{}, errors.New("Error in rule : " + ruleValue)
		}
		if len(ruleParse)-1 == i {
			return Rule{}, errors.New("Predicate or expression missing in rule : " + ruleValue)
		}

		cond.Module = modAndPre[0]
		cond.Predicate = modAndPre[1]
		cond.Expression = ruleParse[i+1]
		myRule.Conditions = append(myRule.Conditions, cond)
	}
	return myRule, nil
}

// isIndent Check if a line is well indent according to an indentValue, return 0
// if it is the same indentation
// return 1 if the current line is more indent than the indentValue
// return -1 if the current line is less indent than the indentValue
func isIndent(line string, indentValue string) int {
	if a := len(indentValue); len(line) >= a+1 && line[0:a] == indentValue {
		if string(line[a]) != " " && string(line[a]) != "\t" {

			return 0
		} else {
			return 1
		}

	} else {
		return -1
	}
}

// getIndentValue read the line and return only the indentation
func getIndentValue(line string) string {
	var indentValue string
	if string(line[0]) == "\t" {
		indentValue = ""
		for i := 0; string(line[i]) == "\t"; i++ {
			indentValue += "\t"
		}
	} else {
		indentValue = ""
		for i := 0; string(line[i]) == " "; i++ {
			indentValue += " "
		}
	}
	return indentValue
}

// parseChain take the file in parameter and a line, it will call parseRules for
// each rules in the current chain
func parseChain(file []string, currentZone *Zone, line int) error {
	chainName := strings.Split(strings.TrimSpace(file[line]), " ")[1]
	currentChain := new(Chain)
	currentChain.Name = chainName
	if len(file[line:]) > 1 && getIndentValue(file[line+1]) != "" {
		indentValue := getIndentValue(file[line+1])
		for i := line + 1; len(file) > i && isIndent(file[i], indentValue) == 0; i++ {
			value, err := parseRules(file[i])
			if err != nil {
				return err
			}
			currentChain.Rules = append(currentChain.Rules, value)
		}

		currentZone.Chains = append(currentZone.Chains, *currentChain)
	} else {
		return errors.New("The chain : \"" + file[line] + "\" need at least one action.")
	}
	return nil
}

// checkConf will check if the configuration specify by the user is correct
func checkConf(configuration Configuration) error {
	for _, zone := range configuration.Zones {
		if len(zone.Interfaces) == 0 {
			return errors.New("The zone : \"" + zone.Name + "\" must have at least one interface")
		}

		validChain := make(map[string]bool)
		validChain[Drop] = true
		validChain[Accept] = true
		for _, e := range zone.Chains {
			validChain[e.Name] = true
		}
		for _, value := range zone.Chains {
			for _, v := range value.Rules {
				if !validChain[v.Action] {
					return errors.New("The chain \"" + v.Action + "\" is call but never implemented.")
				} else if value.Name == v.Action {
					return errors.New("The chain \"" + v.Action + "\" call itself.")
				}
			}
		}
	}
	return nil
}

// parseZone will parse each zone in a ZONE struct
func parseZone(confFile []string, conf *Configuration, line int) error {
	zoneName := strings.Split(confFile[line], " ")[1]
	currentZone := new(Zone)
	currentZone.Name = zoneName
	currentZone.Chains = []Chain{}
	if len(confFile[line:]) > 1 && getIndentValue(confFile[line+1]) != "" {
		indentValue := getIndentValue(confFile[line+1])

		for i := line + 1; len(confFile) > i && isIndent(confFile[i], indentValue) != -1; i++ {
			if isIndent(confFile[i], indentValue) == 0 {
				lineToParse := strings.Split(strings.TrimSpace(confFile[i]), " ")
				switch lineToParse[0] {
				case "IFACES":
					currentZone.Interfaces = lineToParse[1:]
					break
				case "CHAIN":
					err := parseChain(confFile, currentZone, i)
					if err != nil {
						return err
					}
					break
				default:
					return errors.New("Error : \"" + lineToParse[0] + "\" is not a zone option")
				}
			}
		}
		if len(currentZone.Chains) == 0 {
			return errors.New("The zone : \"" + confFile[line] + "\" need at least one chain.")
		}
		conf.Zones = append(conf.Zones, *currentZone)
	} else {
		return errors.New("The zone : \"" + confFile[line] + "\" need at least one interface.")
	}
	return nil
}

// Parse builds a Configuration struct with the bare content of a configuration
// file
func Parse(content string) (Configuration, error) {
	var configuration Configuration
	configuration.Zones = []Zone{}
	confFile := getConf(content)
	for i := 0; i < len(confFile); i++ {
		if string(confFile[i][0]) != " " && string(confFile[i][0]) != "\t" {
			err := parseZone(confFile, &configuration, i)
			if err != nil {
				return Configuration{}, err
			}
		}
	}
	err := checkConf(configuration)
	if err != nil {
		return Configuration{}, err
	}
	return configuration, nil
}
