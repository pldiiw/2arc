/*
Package firewall is a Linux firewall running in user-space

Overview

This package contains an implementation of a firewall running in user-land, aimed
at Linux-based operating systems. It can filter packets flowing in the kernel,
live, and also off the wire, given a pcap-formatted capture file.

Thanks to an expressive configuration format, the user can use expressive rules
to tell the firewall which packet to drop, which to accept.

Adding new protocols for extending the filtering ruleset is easy. Simple
conventions enable anyone to write support for one. Currently, the following
protocols are supported:
 * Ethernet (only in pcap mode)
 * IPv4
 * IPv6
 * TCP
 * UDP

Also, the concept of "zones" adds capabilities to tell which network interface
should be filtered by which rules.

Requirements

This firewall runs on any linux distribution with the Netfilter kernel subsystem
and nftables installed.

On Ubuntu for example, you simply have to `apt install nftables` to get up and running.

The build dependencies require a Go runtime. You can follow the instructions on
the Go official website to install it: https://golang.org/doc/install

Build

After Go is installed correctly, copy the project in the src directory of your $GOPATH, cd into it and run `go get -d ./...` to get all dependencies. Here's a list of used dependencies:

 github.com/google/gopacket
 github.com/subgraph/go-nfnetlink
 github.com/pkg/errors

Next build it with `go build` and `go install` and you should find an executable
in the $GOPATH/bin folder.

Usage

Given a compiled executable of this package named `firewall`, running the
following command will show the help:

 $ ./firewall -help
 Usage of ./2arc:
   -conf string
         The configuration file to use (default "./config.conf")
   -pcap
         If true, the firewall will filter pcap-formatted packets, instead of doing live filtering with the kernel.
   -pcap-file string
         The capture file to read packets from, used only when -pcap is set to true. (default "/dev/stdin")

If you specify the `-pcap` option, then packets will be read from the specified
pcap-formatted file by the -pcap-file option, defaulting to stdin. The firewall
will output for each packet if it would accept it or drop it as if the traffic
was live. At the end, a summary with some statistics is displayed.

Otherwise, the firewall will be setup for live operation. It will parse the
configuration file indicated by the `-conf` option, prepare the filtering
pipelines, then hooks itself to the kernel's network flow, ready to receive
packets and to give verdicts.

Whatever operation mode is chosen, the program needs a configuration file that
will describes in a user-friendly format how the packets should treated
depending on their content.

Configuration

When the firewall is started, it loads a given plain-text configuration file
that, after it's been parsed, indicates how to handle each packet, what to
accept and what to drop.

We will explain in this section how to write such a configuration file.

As a direct example, here's an dummy configuration file:

 ZONE default
 	IFACES *
 	CHAIN in
        	DROP ipv4/SourceNot 192.168.1.2 tcp/Destination 22
        	ACCEPT

This configuration file tells the firewall to drop all incoming packets
targeting the port 22 not coming from the IP address 192.168.1.2. Otherwise, the
packet is accepted.

As you see, sections are nested via literal tabs. The leftmost section is the
ZONE. You can create as many zone as you cant. Each ZONE has a name, here it's
"default". Only the default ZONE is used by the firewall, any other zone is here
to help you organize your setups in one configuration file instead of many.

Next up, the IFACES section, subsection of ZONE. Originally it was intended to
tell which interface the ZONE would act upon, but due to the limitations of
libnetfilter_queue, it only here as an artefact. You can put anything next to
up, but a good practice is use a '*' to select all interfaces. This won't break
anything if one day support for NIC selection is added.

Afterwards, there's the CHAIN sections. Here there's only one but you can add as
many as you want. A CHAIN groups an ordered flow of rules to filter packets and
assign it a name. Here's the name is "in", a builtin name. There's three builtin
names: in, out, and forward. The "in" CHAIN receives all packets which are
destined to you. The "out" for packets you initiate, and "forward" for packets
which aren't set to be delivered to you and you did not send.

You can also create custom CHAINs. Custom chains can be used as actions in
rules, as we're going see below.

A CHAIN is composed of rules. A rule has the following format:

 <action> [<module>/<predicate> <expression>]...

Let start with the module/predicate/expression triplet. A module is the name of
an implemented protocol supported by our firewall (see next section for more
insight). Each module exposes a set of predicates. A predicate is the piece that
let you match packets. It's just a function, that, given an expression, will
inspect the packets that flow through its rule, extract some fields, compare it
with the expression and matches or doesn't. In our dummy example, we have the
SourceNot predicate for instance, from the ipv4 module. When looking up its
documentation we can learn that it expects a single IP address as the expression
and compare it to the traversing packet's source IP field, matching when the
source IP is not the same as the expression. The expression is
predicate-specific. It's just a suite of characters that should match what the
predicate expects, may it be a port, an address IP or even an ICMP type. If you
mistakenly write a malformed expression, the firewall will tell you right away
and even print out a message indicating what it expected.

All module/predicate/expression in a rule are ANDed with each other, so a packet
will match the rule only when all triplets matches.

When all predicates matches, the rule's action is executed. There's two builtin
action: ACCEPT and DROP. ACCEPT will accept the packet and stop the filtering
processing for it. DROP is the complement, dropping the packet. If a custom
action is specified, the filtered packet will jump to the CHAIN holding the
action's name in the current ZONE, traverse it, and then return, passing to the
next rule if no rules matched during the jump. This feature will help you
organize your rules into generic filtering pipelines for your own use cases.

If no module/predicate/expression triplet is given, the rule unconditionaly
matches. This is why such a rule is put at the end of our "in" CHAIN, so that
our packets always end up ACCEPTed if no rules matched before. You can think of
it as the default policy for the CHAIN.

You can also write comments anywhere with a #. They end up at the end of the
line they begin. Everything between will be discarded when parsing.

This pretty much wraps up how to write a configuration file.

Available Modules

Currently, a total of 6 protocols are supported:
 * Ethernet
 * IPv4
 * IPv6
 * ICMPv4
 * TCP
 * UDP

Each of these modules has many predicates to use. Refer to their documentation
for further information.

Writing A New Module

You can easily implement new modules to support new protocols.

Go take a look at one of modules in the modules folder, the structure is very
simple.

Each predicate in the configuration file is in fact just an exported function
from the package holding the same name as the module. Each predicate has the
following signature:

 func Predicate(expr string) (func(gopacket.Packet) bool, error)

The predicate is a function taking as parameter an expression, and returning a
new function that treats gopackets, telling if the packet matches or not
as a bool. An error should also be returned when the expression that was given
to you isn't parseable.

Once, you've written your predicates, you need export it. For this, each module has a Predicate map which maps predicate names to be used in configuration files and the functions you've just written. It's type signature is:

 var Predicates = map[string]func(expr string) (func(gopacket.Packet) bool, error){}

Once exported, you need to tell the code that initialize the filtering pipelines
from the parsed configuration to import it. Head to the pipeline package and add
your module to the modules map, as such:

 import (
         ...
 	"2arc/modules/stub"
         ...
 )

 var modules = map[string](map[string]func(string) (func(gopacket.Packet) bool, error)){
 	"stub": stub.Predicates,
 }

Technical Details

The following sections deep dive into the technical details of the application.

Queuing Packets To User-Space

In order to influence packets flowing through the kernel from userspace, we need
to leverage some kernel API. The one that has been chosen is the nfqueue API. It
is provided by the Netfilter kernel subsystem. Nfqueue enables to create an
nfnetlink socket from userspace through which packets in kernel will be copied.
This socket has special commands to tell the kernel to accept or drop a packet
that was previously copied to us. We can create an nfnetlink socket and attach
it to any nf_hook in the kernel, these nf_hooks being the mean that many
firewalls uses to gain traction on network flow.

Registering an nfnetlink socket as an nf_hook isn't a direct operation, we need
an intermediary to do the job for us. This is where nftables comes to rescue.
When the firewall is started in live mode, it executes some shell commands to
creates "queue" rules in nftables. For an example of the usage of nftables in
this case, this wiki page was used:
https://wiki.nftables.org/wiki-nftables/index.php/Queueing_to_userspace

This way, we create 3 nfqueues. One on the NF_IP_LOCAL_IN hook, one on the
NF_IP_FORWARD hook, and one on NF_IP_LOCAL_OUT hook. We then connect our
nfnetlink sockets to these, and from here we start receiving our packets.

One issue with nfqueues is that there's no xtables kernel subsystem that would
let us receive raw Ethernet frames. When we receive our packets in userspace,
the link layer has been replaced by a netlink layer. This means that the
first meaningful layer we can use is the network layer. The firewall is fully
capable is filtering ethernet frames, but in live operation this is sadly not
possible with the mechanism we used.

Another API we could have used is the Data Plane Development Kit, DPDK. It's a
high performance API that runs in userland, but its existence was discovered too
late in the project and its API is too gargantuan to master it in a short time
just gigantic.

From Configuration To Operation — How We Parse Rules

To setup the filtering pipelines from a plain text configuration file, some
steps need to be taken. This is two step process.

The first step is reading the configuration file and parsing it into a data
structure that can be used by other parts of the code. The config package
manages this operation. Here's a complete description of the process:

First of all, every blank line and comment are deleted. The content is sent to a
ZONE parsing function, checking if a line begins by the word "ZONE". If it's the
case it creates a new Zone struct. Then, the following line is checked for
indentation, and, depending on the first word it finds, namely "IFACE" or
"CHAIN", the right function is called.

The function parsing "IFACES" uses the space between each interface's name to
separate them.

CHAINs, like ZONEs, when encountered, creates a new Chain struct.

The function in charge of rule parsing creates a new rule for each line
following the creation of the chain. The process stops when the line is less
indented than the preceding line. When the rule is created the function searches
first for the module and predicate separate by a slash, then the intended
expression. If it's not the end of the line it will create another triplet.

The parsed configuration in a data structure composed of the following structs:

 type Configuration struct {
 	Zones []Zone
 }

 type Zone struct {
 	Name       string
 	Interfaces []string
 	Chains     []Chain
 }

 type Rule struct {
 	Action     string
 	Conditions []Condition
 }

 type Chain struct {
 	Name  string
 	Rules []Rule
 }

 type Condition struct {
 	Module     string
 	Predicate  string
 	Expression string
 }

You can find these structs in the config package.

When the function is done parsing it will check if the struct doesn't contain
any errors, e.g. a CHAIN calling itself.

When the parsing is done, the pipeline package takes care of manipulating this
data structure into the filtering pipelines. These take care of instanciating
the right predicates, forming rules, chains and zones by chaining multiple
functions. There's a 1:1 mapping to CHAINs and rules between the parsed
configuration and the generated functions.

Matching Packets

When a predicate try to match a packet, the layer handled by the module is
extracted, its field compared to a parsed expression, returning a bool
representing whether the packet matched the predicate or not. The above sections
explain it bit by bit.
*/
package firewall
